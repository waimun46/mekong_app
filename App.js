/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, AsyncStorage, TextInput } from 'react-native';
import { Icon, StyleProvider, Container, Root } from 'native-base';
import getTheme from './src/themes/components';
import appColor from './src/themes/variables/appColor';
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import IconANT from "react-native-vector-icons/AntDesign";
import IconMAT from "react-native-vector-icons/MaterialIcons";
import IconFA from "react-native-vector-icons/FontAwesome";
import IconFOU from "react-native-vector-icons/Foundation";
import IconOCT from "react-native-vector-icons/Entypo";


import { Router, Scene, Actions, Lightbox } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

import WelcomePage from './src/components/pages/welcome';
import LoginModal from './src/components/pages/login';
import GdSeries from './src/components/pages/gdseries';
import GDPremium from './src/components/pages/gdseries/components/gdpremium';
import CountryInfor from './src/components/pages/gdseries/components/components/countryinfor';
import CountryInforList from './src/components/pages/gdseries/components/components/countryinforlist';
import HomesScreen from './src/components/pages/home';
import InforScreen from './src/components/pages/infor';
import ProfilePage from './src/components/pages/profile';
import Newspage from './src/components/pages/news';
import Agent from './src/components/pages/agent';
import AgentInfor from './src/components/pages/agent/components/agentinfor';
import DepartureLogin from './src/components/pages/departure/departurelogin';
import DepartureInfor from './src/components/pages/departure';
import ViewInfor from './src/components/pages/news/components/components/viewinfor';
import EmailVailed from './src/components/pages/emailvailed';
import RegisterScreen from './src/components/pages/register';
import ScanPage from './src/components/pages/scan';
import ViewDetailData from './src/components/pages/gdseries/components/components/viewdetailsData';
import CompanyProfile from './src/components/pages/infor/components/companyProfile';
import Feedback from './src/components/pages/infor/components/feedback';
import Privacy from './src/components/pages/infor/components/privacy';
import TermsCondition from './src/components/pages/infor/components/terms';
import GDConsortiums from './src/components/pages/infor/components/gdconsortiums';
import ViewDetailUpcoming from './src/components/pages/home/components/components/viewDetail';
import ModalScreen from './src/components/pages/departure/components/components/modalScreen';
import ChangePassword from './src/components/pages/profile/components/changePass';
import Reference from './src/components/pages/infor/components/ReferenceCertificates'
import CustomerReference from './src/components/pages/infor/components/CustomerReference';
import StructureOffice from './src/components/pages/infor/components/StructureOffice';
import HightLineData from './src/components/pages/home/components/components/hightlineData'

/************************************ Not allow Font Scaling when Phone Setting font size ***********************************/
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

/************************************ Not allow Text input font Scaling when Phone Setting font size  ***********************************/
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

/************************************ Not allow HTML api font Scaling when Phone Setting font size  ***********************************/
HTML.defaultProps = HTML.defaultProps || {};
HTML.defaultProps.allowFontScaling = false;




const iconLogo = () => (
  <Image source={require('./src/assets/images/logo_r.png')} style={{ width: 60, height: 60, marginTop: -30 }} />
)

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? 'yellow' : '#d6cccc' }}>{title}</Text>
  );
}

const Rightbtn = () => {
  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <IconMCI name='qrcode-scan' style={{ fontSize: 22, paddingRight: 15, color: '#fff' }} onPress={() => Actions.scan()} />
      <IconOCT name='info' style={{ fontSize: 22, paddingRight: 15, color: '#fff' }} onPress={() => Actions.Infor()} />
      <IconMCI name='account' style={{ fontSize: 25, paddingRight: 10, color: '#fff' }} onPress={() => Actions.profile()} />
    </View>
  );
}

class App extends Component {



  render() {

    return (

      <Container >

        <StatusBar barStyle="light-content" backgroundColor="#24292e" />
        <Root>
          <Router hideNavBar navigationBarStyle={{ backgroundColor: '#24292e' }} iosBarStyle={"light-content"} tintColor='#ccc'>
            <Lightbox >
              {/* 
              <Scene key="modal" modal   component={ModalScreen} hideTabBar={true}
                direction="vertical" hideNavBar={true} navTransparent={true}
                sceneStyle={{ backgroundColor: 'transparent' }} headerMode='screen' /> */}

              <Scene key="root" hideNavBar allowFontScaling={false} headerTitleAllowFontScaling={false}>

                {/*               
              <Scene key="email" component={EmailVailed} hideNavBar={false} />
               
              */}
                <Scene key="welcome" component={WelcomePage} initial={true} hideNavBar />


                <Scene
                  headerLayoutPreset="left"
                  tabs={true}
                  icon={TabIcon}
                  titleStyle={{ color: '#fff', fontSize: 16 }}
                  tabBarStyle={{ backgroundColor: '#de2d30' }}
                  swipeEnabled={true}
                  activeTintColor={'yellow'}
                  key="Homepage"
                  inactiveTintColor="#fff"
                  activeTintColor="yellow"
                >






                  {/************************  home ************************/}
                  <Scene
                    tabBarLabel={global.t('Home')}
                    icon={({ focused }) => (
                      <IconFA name='home' size={25} style={{ color: focused ? 'yellow' : '#fff' }} />
                    )}
                    key="Home"

                  >

                    <Scene key="Home" initial={true} renderBackButton={() => (null)} renderLeftButton={() => (null)} component={HomesScreen}
                      title={global.t('ikhmertrip')} renderRightButton={Rightbtn} />
                    <Scene headerLayoutPreset="center" titleStyle={{ color: 'white', fontSize: 16 }} tabBarStyle={{ backgroundColor: '#fcf5f5' }}
                      swipeEnabled={true} activeTintColor={'#de2d30'} key="register" component={RegisterScreen} hideTabBar={true}
                    />
                    <Scene key="gdSeries" component={GdSeries} title={global.t('Series_Tour')} renderRightButton={Rightbtn} />
                    <Scene key="countryinforlist" component={CountryInforList} title={global.t('Package_Tour')} renderRightButton={Rightbtn} />
                    <Scene key="upcomingdetail" component={ViewDetailUpcoming} title={global.t('Upcoming_Package')} renderRightButton={Rightbtn} />
                    <Scene key="hightLine" component={HightLineData} title={global.t('Package_List')} renderRightButton={Rightbtn} />

                    {/*--- Rightbtn Screen ---*/}
                    <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                    <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                    <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} />
                    <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                    {/*--- Infor Screen content ---*/}
                    <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                    <Scene key="reference" component={Reference} title={global.t('Reference_Certificates_Partner')} hideTabBar={true} />
                    <Scene key="customer" component={CustomerReference} title={global.t('Customer_Reference')} hideTabBar={true} />
                    <Scene key="structure" component={StructureOffice} title={global.t('Structure_Office')} hideTabBar={true} />
                    <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                    <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                    <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                    <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                    <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />

                  </Scene>



                  {/************************  News ************************/}
                  <Scene
                    tabBarLabel={global.t('News')}
                    icon={({ focused }) => (
                      <IconFA name='rss' size={25} style={{ color: focused ? 'yellow' : '#fff' }} />
                    )}
                  >
                    <Scene key="News" component={Newspage} initial={true} title={global.t('News')} renderRightButton={Rightbtn} />
                    <Scene key="viewinfor" component={ViewInfor} title={global.t('News_Information')} renderRightButton={Rightbtn} />


                    {/*--- Rightbtn Screen ---*/}
                    <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                    <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                    <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} />
                    <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                    {/*--- Infor Screen content ---*/}
                    <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                    <Scene key="reference" component={Reference} title={global.t('Reference_Certificates_Partner')} hideTabBar={true} />
                    <Scene key="customer" component={CustomerReference} title={global.t('Customer_Reference')} hideTabBar={true} />
                    <Scene key="structure" component={StructureOffice} title={global.t('Structure_Office')} hideTabBar={true} />
                    <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                    <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                    <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                    <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                    <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                  </Scene>



                  {/************************  Package ************************/}
                  <Scene
                    tabBarLabel={global.t('Package')}
                    icon={({ focused }) => (
                      <IconFA name='shopping-bag' size={25} style={{ color: focused ? 'yellow' : '#fff' }} />
                    )}
                  >
                    <Scene key="gdSeries" initial={true} component={GdSeries} title={global.t('Series_Tour')} renderRightButton={Rightbtn} />
                    {/* <Scene key="gdPremium" component={GDPremium} title={global.t('Series_Selection')} renderRightButton={Rightbtn} /> */}
                    <Scene key="countryinfor" component={CountryInfor} title={global.t('Country_Information')} renderRightButton={Rightbtn} />
                    <Scene key="countryinforlist" component={CountryInforList} title={global.t('Package_Tour')} renderRightButton={Rightbtn} />
                    <Scene key="viewdetails" component={ViewDetailData} title={global.t('Package_List')} renderRightButton={Rightbtn} />

                    {/*--- Rightbtn Screen ---*/}
                    <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                    <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                    <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} />
                    <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                    {/*--- Infor Screen content ---*/}
                    <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                    <Scene key="reference" component={Reference} title={global.t('Reference_Certificates_Partner')} hideTabBar={true} />
                    <Scene key="customer" component={CustomerReference} title={global.t('Customer_Reference')} hideTabBar={true} />
                    <Scene key="structure" component={StructureOffice} title={global.t('Structure_Office')} hideTabBar={true} />
                    <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                    <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                    <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                    <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                    <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                  </Scene>





                  {/************************  Agent ************************/}
                  <Scene
                    tabBarLabel={global.t('Agent')}
                    icon={({ focused }) => (
                      <IconFOU name='torsos-all' size={25} style={{ color: focused ? 'yellow' : '#fff' }} />
                    )}
                  >
                    <Scene key="Agent" component={Agent} title={global.t('Agent')} initial={true} renderRightButton={Rightbtn} />
                    <Scene key="Agentinfor" component={AgentInfor} title={global.t('Agent_Information')} renderRightButton={Rightbtn} />

                    {/*--- Rightbtn Screen ---*/}
                    <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                    <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                    <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} />
                    <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                    {/*--- Infor Screen content ---*/}
                    <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                    <Scene key="reference" component={Reference} title={global.t('Reference_Certificates_Partner')} hideTabBar={true} />
                    <Scene key="customer" component={CustomerReference} title={global.t('Customer_Reference')} hideTabBar={true} />
                    <Scene key="structure" component={StructureOffice} title={global.t('Structure_Office')} hideTabBar={true} />
                    <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                    <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                    <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                    <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                    <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                  </Scene>



                  {/************************  Departure ************************/}
                  <Scene
                    tabBarLabel={global.t('Departure_Info')}
                    icon={({ focused }) => (
                      <IconMAT name='flight-takeoff' size={25} style={{ color: focused ? 'yellow' : '#fff' }} />
                    )}
                  >
                    <Scene key="Departurelogin" renderBackButton={() => (null)} renderLeftButton={() => (null)} initial={true} component={DepartureLogin} title={global.t('Departure_Login')} renderRightButton={Rightbtn} />
                    <Scene key="Departure" renderBackButton={() => (null)} renderLeftButton={() => (null)} component={DepartureInfor} title={global.t('Departure_Info')} renderRightButton={Rightbtn} />

                    {/*--- Rightbtn Screen ---*/}
                    <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                    <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                    <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} />
                    <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                    {/*--- Infor Screen content ---*/}
                    <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                    <Scene key="reference" component={Reference} title={global.t('Reference_Certificates_Partner')} hideTabBar={true} />
                    <Scene key="customer" component={CustomerReference} title={global.t('Customer_Reference')} hideTabBar={true} />
                    <Scene key="structure" component={StructureOffice} title={global.t('Structure_Office')} hideTabBar={true} />
                    <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                    <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                    <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                    <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                    <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                  </Scene>





                </Scene>



              </Scene>
            </Lightbox>
          </Router>
        </Root>
      </Container>


    );
  }
}
export default App;

