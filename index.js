/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './redux/store'

import {PersistGate} from 'redux-persist/integration/react'

const {store, persistor}  = configureStore()

const onBeforeLift = () => {

}

const myProvider = () => {
  return (
    <Provider store = {store}>
      <PersistGate loading={null} persistor={persistor}
        onBeforeLift={onBeforeLift}
      >
        <App />
      </PersistGate>
    </Provider>
  )
}



AppRegistry.registerComponent(appName, () => myProvider);
