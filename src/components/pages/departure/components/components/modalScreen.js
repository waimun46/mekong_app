import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, Text, Modal, Platform, TextInput, Toast } from 'react-native';
import {
    Container, Header, Content, Accordion, Form, Item, Input, Label, Footer,
    Button, List, ListItem, Icon, Left, Body, Right, Switch
} from "native-base";
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";


class ModalScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
            isModalVisible: true,
            isModalVisibleClose: false,
            email: '',
            username: '',
            password: '',
            Error: '',
            data: [],
            defaultAnimationDialog: true,
        };
    }


    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    _closeModal = () => {
        this.setState({ isModalVisible: false });
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'username') { this.setState({ username: text }) }
        if (field == 'password') { this.setState({ password: text }) }
        if (field == 'email') { this.setState({ email: text }) }
    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(username = this.state.username, password = this.state.password, email = this.state.email) {
        console.log(username, password, '----------------username,password')

        /****************************** Validations form Toast *****************************/
        if (this.state.username === "") {
            this.setState({
                Error: Toast.show({
                    text: 'Please Fill In The Username', buttonText: 'Okay', position: "top", duration: 3000, textStyle: { color: "yellow" },
                })
            })
        }
        else if (this.state.email === "") {
            this.setState({
                Error: Toast.show({
                    text: 'Please Fill In The Email', buttonText: 'Okay', position: "top", duration: 3000, textStyle: { color: "yellow" },
                })
            })
        }
        else if (this.state.password === "") {
            this.setState({
                Error: Toast.show({
                    text: 'Please Fill In The Password', buttonText: 'Okay', position: "top", duration: 3000, textStyle: { color: "yellow" },
                })
            })
        }
        else {
            this.setState({ Error: '' })
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/login.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&email=${email}&username=${username}&password=${password}`;

            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, 'myJson-----------');
                    if (myJson[0].status == 1) {
                        return myJson.json(this.setState({ isModalVisible: false }))

                    } else {
                        alert(myJson[0].error);
                    }
                });
        }
    }

    render() {
        return (
            <View style={styles.shadow}>
                <View style={styles.container}>

                </View>
            </View>

        );
    }
}


export default ModalScreen;

const styles = StyleSheet.create({

    contentWarp: {width: '100%', marginLeft: 'auto', marginRight: 'auto',},
    contentwarp2: {width: '100%', marginLeft: 'auto', marginRight: 'auto',padding: 15},
    logoImage: { resizeMode: 'contain', width: '90%', marginTop: 10, height: 150, textAlign: 'center', marginLeft: 'auto', marginRight: 'auto' },
    labelstyle: { color: '#ffffffad', width: '30%', paddingTop: 10, paddingBottom: 10 },
    inputstyle: { color: 'white', paddingLeft: 20, width: '70%', height: 30, },
    warpperstyle: {
        width: '100%', backgroundColor: '#de2d30',
        borderTopLeftRadius: 10, borderTopRightRadius: 10, padding: 30, width: '100%'
    },

    // container: {
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    dialogContentView: {
        // flex: 1,
        paddingLeft: 18,
        paddingRight: 18,
        // backgroundColor: '#000',
        // opacity: 0.4,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    navigationBar: {
        borderBottomColor: '#b5b5b5',
        borderBottomWidth: 0.5,
        backgroundColor: '#ffffff',
    },
    navigationTitle: {
        padding: 10,
    },
    navigationButton: {
        padding: 10,
    },
    navigationLeftButton: {
        paddingLeft: 20,
        paddingRight: 40,
    },
    navigator: {
        flex: 1,
        // backgroundColor: '#000000',
    },
    customBackgroundDialog: {
        opacity: 0.5,
        backgroundColor: '#000',
    },

    shadow: {
    backgroundColor: 'rgba(52,52,52,0.5)', // 透明な背景
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    width: 300,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
})
