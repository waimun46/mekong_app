import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconION from "react-native-vector-icons/Ionicons";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import HTML from 'react-native-render-html';
import { DepartingDetailApi } from '../../../../../PostApi'


class InformationPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            AsyncStorageId: 0,
            langPrint: ''
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            console.log(asyncStorageRes, '----------------AsyncStorage-----Note')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            console.log(this.state.AsyncStorageId, '-------------fetchDataApi----Note')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                console.log(fetchData, '-----------------fetchData-----Note')
                this.setState({
                    data: fetchData[0]
                })
            })

        });

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----InformationPage')
            this.setState({
                langPrint: langStorageRes
            })
        })



    }

    render() {
        const { data, langPrint } = this.state;
        return (
            <View style={{ backgroundColor: 'white', padding: 25, }}>
                <View >
                    <Text style={styles.titletext}>{global.t('Important_Information')}: </Text>
                </View>
                <View style={{ marginTop: 20 }}>
                    {
                        langPrint === 'zh' ? (
                            <HTML
                                html={data.info_cn}
                                tagsStyles={{
                                    p: { marginBottom: 15 },
                                }}
                            />
                        ) : (
                                <HTML
                                    html={data.info_en}
                                    tagsStyles={{
                                        p: { marginBottom: 15 },
                                    }}
                                />
                            )
                    }
                </View>

            </View>
        );
    }
}


export default InformationPage;

const styles = StyleSheet.create({
    titletext: { color: '#de2d30', fontSize: 18, fontWeight: 'bold', },
    textstyle: { color: '#9e9ea6', fontSize: 16, marginBottom: 15 }
})
