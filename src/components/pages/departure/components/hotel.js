import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, AsyncStorage, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { DepartingDetailApi } from '../../../../../PostApi';
import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';



class HotelPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            AsyncStorageId: 0,
            isLoding: true,
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            console.log(asyncStorageRes, '----------------HotelPage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------HotelPage')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                console.log(fetchData, '-----------------HotelPage')
                this.setState({
                    data: fetchData[0].hotel_detail,
                    isLoding: false
                })
            })
            // this.fetchDataApi(this.state.AsyncStorageId);
        });


    }

    keyExtractor = (item, index) => item.key;

    renderItem = ({ item, index }) => {
        return (

            <Card style={{ marginBottom: 20 }}>
                <CardItem style={{ borderWidth: .5, borderColor: '#ccc' }}>
                    <Left>
                        <Body style={{ marginLeft: 0 }}>
                            <Text style={styles.rightcontenttitle}>{item.hotelname}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Body style={styles.bodystyle}>
                        <View style={styles.addressstyle}>
                            <Text style={styles.righttitletext}>{global.t('Address')}: </Text>
                            <Text style={styles.righttext}>{item.hoteladdress} </Text>
                        </View>
                        <View style={styles.phonestyle}>
                            <Text style={styles.righttitletext}>{global.t('Phone_no')}: </Text>
                            <Text style={styles.righttext}>{item.telnum}</Text>
                        </View>
                    </Body>

                </CardItem>
                <CardItem style={styles.cardbottom}>

                    <View style={{ width: '50%', flexDirection: 'row' }}>
                        <View style={[styles.checkwarptext, { backgroundColor: '#fcf5f5' }]}>
                            <Text style={[styles.checkiconstyle, { color: 'green' }]}>{global.t('IN')}</Text>
                        </View>

                        <View style={styles.checkwarp}>
                            <Text style={{ textAlign: 'center' }}>{item.checkin}</Text>
                        </View>
                    </View>

                    <View style={{ width: '50%', flexDirection: 'row' }}>
                        <View style={[styles.checkwarptext, { backgroundColor: '#fcf5f5' }]}>
                            <Text style={[styles.checkiconstyle, { color: '#e87171' }]}>{global.t('OUT')}</Text>
                        </View>

                        <View style={styles.checkwarp}>
                            <Text style={{ textAlign: 'center' }}>{item.checkout}</Text>
                        </View>
                    </View>
                </CardItem>
            </Card>
        )
    }

    ListEmpty = () => {
        return (
            <View style={styles.MainContainer}>
                <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
            </View>
        );
    };

    render() {
        const { data, isLoding } = this.state;
        return (
            <View style={{ backgroundColor: '#e9ecef', }}>
                {
                    isLoding ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                            <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                            <ActivityIndicator size="large" color="#de2d30" />
                        </View>
                    ) : (
                            <View style={{ padding: 10 }}>
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}
                                    keyExtractor={this.keyExtractor}
                                    style={{ paddingBottom: 50 }}
                                    ListEmptyComponent={this.ListEmpty}
                                />
                            </View>
                        )
                }

            </View>
        );
    }
}


export default HotelPage;

const styles = StyleSheet.create({
    MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
    rightcontenttitle: { fontSize: 16, fontWeight: 'bold', color: '#de2d30', textAlign: 'center' },
    righttitletext: { width: '35%', fontWeight: 'bold', color: 'black', fontSize: 14, },
    righttext: { width: '65%', fontSize: 14, },
    bodystyle: { paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10 },
    addressstyle: { flex: 1, flexDirection: 'row', width: '100%', marginBottom: 10 },
    phonestyle: { flex: 1, flexDirection: 'row', width: '100%' },
    cardbottom: {
        marginTop: 10, backgroundColor: '#eee', paddingLeft: 0, paddingRight: 0,
        paddingTop: 0, paddingBottom: 0
    },
    checkwarp: {
        flexDirection: 'row', width: '50%', marginLeft: 'auto', marginRight: 'auto',
        paddingLeft: 1, paddingRight: 1, paddingTop: 10, paddingBottom: 10
    },
    checkiconstyle: { textAlign: 'center', fontWeight: 'bold', },
    checkwarptext: { padding: 10, width: '30%' }

})



