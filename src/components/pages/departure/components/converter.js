import React, { Component } from 'react';
import {
    StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, TextInput, ActivityIndicator,
    Keyboard, TouchableWithoutFeedback, TouchableOpacity, Modal, TouchableHighlight
} from 'react-native';
import {
    Container, Header, Content, Accordion, Form, Item, Input, Label, Footer,
    Button, List, ListItem, Icon, Left, Body, Right, Switch
} from "native-base";
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { Select, Option } from "react-native-chooser";
import WeatherPage from './components/weather';
import { nullLiteral } from '@babel/types';

// const DismissKeyboard = ({ children }) => (
//     <TouchableWithoutFeedback onPress={() => Keyboard.dismiss} accessible={false}>
//         {children}
//     </TouchableWithoutFeedback>
// );



class ConverterPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            latitude: null,
            longitude: null,
            error: null,
            isLoading: true,
            temperature: 0,
            weatherCondition: null,
            weatherIcon: null,
            data: [],
            currencies: rateData.currencies,
            currencyA: rateData.currencies[0],
            currencyB: rateData.currencies[1],
            currencyAval: rateData.currencies[0].sellRate,
            currencyBval: rateData.currencies[1].sellRate,
            rateApi: [],
            value: rateData.currencies[1].code,
            text: '',
            totalnum: '',
            text2: '',
            isLoading: true,
            modalVisible: false,
            dataLocal: []


        }


        this.onSelectCurrency = this.onSelectCurrency.bind(this);
        //this.onChangeHandler = this.onChangeHandler.bind(this);
    }





    // toggleModal(visible) {
    //     this.setState({ modalVisible: visible });
    // }

    // onChangeHandler(e, currency) {
    //     const { currencyA, currencyB } = this.state;
    //     if (currency === 'A') {
    //         console.log('changingA');
    //         const newValueA = e.target.value;
    //         this.setState({
    //             currencyAval: newValueA,
    //             currencyBval: newValueA * currencyB.sellRate
    //         })

    //     } else if (currency === 'B') {
    //         console.log('changingB');
    //         const newValueB = e.target.value;
    //         this.setState({
    //             currencyAval: newValueB / currencyB.sellRate,
    //             currencyBval: newValueB
    //         })
    //     }
    // }


    componentDidMount() {

        /************************* Get the current position of the user *************************/
        navigator.geolocation.getCurrentPosition(
            position => {
                this.fetchWeather(position.coords.latitude, position.coords.longitude);
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 20000 }
            // error => {
            //     this.setState({
            //         error: 'Error Getting Weather Condtions'
            //     }, {
            //             enableHighAccuracy: false
            //             , timeout: 5000
            //         });
            // }
        );

        /************************* Alert Current Locations *************************/
        navigator.geolocation.getCurrentPosition(
            // function (position) {
            //   alert("Lat: " + position.coords.latitude + "\nLon: " + position.coords.longitude);
            // },
            function (error) {
                alert(error.message);
            }, {
                enableHighAccuracy: false
                , timeout: 20000
            }
        );
    }

    /************************* Fetch the weather api display in state *************************/
    fetchWeather(lat = this.state.latitude, lon = this.state.longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
        fetch(
            `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
        )
            .then(res => res.json())
            .then(json => {
                console.log(json);
                this.setState({
                    temperature: json.main.temp.toFixed(0),
                    weatherCondition: json.weather[0].main,
                    weatherIcon: json.weather[0].icon,
                    data: json,
                    isLoading: false
                });
            });
    }

    // fetchWeather(lat = this.state.latitude, lon = this.state.longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
    //     fetch(
    //         `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    //     )
    //         .then(res => res.json())
    //         .then(json => {
    //             console.log(json);
    //             this.setState({
    //                 temperature: json.main.temp.toFixed(0),
    //                 weatherCondition: json.weather[0].main,
    //                 weatherIcon: json.weather[0].icon,
    //                 data: json,
    //                 isLoading: false
    //             });
    //         });
    // }

    /************************* Select Currency Option *************************/
    onSelectCurrency(code, value) {
        console.log('Selecting :' + code);
        const { currencies, currencyAval } = this.state;
        const currency = currencies.filter(currency => currency.code === code);
        this.setState({
            value: value,
            currencyB: currency[0],
            currencyBval: currencyAval * currency[0].sellRate

        })
    }

    /*************************  count Total submit *************************/
    onSubmit() {
        const { currencyBval, text } = this.state;
        this.setState({
            totalnum: text * currencyBval.toFixed(4)
        })

    }

    onPressModal() {
        this.setState({
            modalVisible: false,
        })
    }

    // renderItem = ({ item }) => {
    //     return (
    //             <ListItem icon onPress={() => this.onPressModal(item)} >
    //                 <Left>
    //                     <Image
    //                         source={{ uri: item.logo }}
    //                         style={{ width: 30, height: 30, resizeMode: 'cover' }}
    //                     />
    //                 </Left>
    //                 <Body>
    //                     <Text>{item.country}</Text>
    //                 </Body>
    //                 <Right>
    //                     <IconANT active name="check" />
    //                 </Right>
    //             </ListItem>

    //     )
    // }


    render() {
        //console.log(this.state.currencyBval, 'currencyBval');
        // console.log(this.state.currencyAval, 'currencyAval')
        const { isLoading, temperature, currencies, currencyA, currencyB, currencyAval, currencyBval, currencyLogo } = this.state;
        const filteredCurrencies = currencies.filter(currency => currency.code !== 'MYR')

        return (
            <View style={{ backgroundColor: 'white', }}>

                <View style={{ backgroundColor: '#8eabcd', }}>
                    {/* <TouchableHighlight onPress={() => { this.toggleModal(true) }}>
                        <Text style={styles.text}>Open Modal</Text>
                    </TouchableHighlight> */}

                    {
                        isLoading ? (
                            <ActivityIndicator size="large" color="#de2d30" />
                        ) : (
                                <View style={styles.degreewarp}>
                                    <View style={{ width: '40%', paddingRight: 5 }}>
                                        <Text style={styles.degreenum}>{temperature}°</Text>
                                    </View>
                                    <View style={{ width: '60%', }}>
                                        <Text style={styles.degreelocation}>{this.state.data.name}</Text>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.degreeweather}>{this.state.weatherCondition}</Text>
                                            <Image
                                                style={{ width: 60, height: 40, }}
                                                source={{
                                                    uri: "https://openweathermap.org/img/w/"
                                                        + this.state.weatherIcon
                                                        + ".png"
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                            )
                    } 

                </View>
                <View style={{ backgroundColor: '#8eabcd', }}><WeatherPage /></View>

                <View style={{ padding: 10, }}>
                    <Text style={styles.convertitle}>{global.t('Currency_Convertor')}</Text>
                </View>

                <Form>
                    <View style={{ backgroundColor: 'black', }}>
                        <Item style={{ borderBottomWidth: 0, padding: 10 }}>

                            <TouchableOpacity>
                                <Select
                                    defaultText={this.state.value}
                                    onSelect={this.onSelectCurrency.bind(this)}
                                    onChange={(e) => this.onSelectCurrency(e.target.value)}
                                    style={{ borderWidth: 0, height: 50, width: 80, paddingTop: 15, padding: 0, }}
                                    textStyle={{ textAlign: 'left', color: '#6ff53c', fontWeight: 'bold', fontSize: 18, }}
                                    backdropStyle={{ backgroundColor: "#00000078" }}
                                    optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, textAlign: 'center', borderWidth: 0, }}
                                    animationType='none'
                                    transparent={true}
                                    indicator='down'
                                    indicatorColor='#6ff53c'
                                    indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                                    indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 5 : 2, textAlign: 'left' }}
                                >
                                    {
                                        filteredCurrencies.map(currency => {
                                            const { code } = currency
                                            return <Option key={code} value={code}>{code}</Option>
                                        })
                                    }
                                </Select>
                            </TouchableOpacity>
                            <TextInput
                                style={{ width: '90%', padding: 10, paddingLeft: 30, fontSize: 18, fontWeight: 'bold', }}
                                placeholderTextColor='#fff'
                                placeholder={this.state.currencyBval.toFixed(4)}
                                onChangeText={(text) => this.setState({ input1: this.state.currencyBval })}
                                keyboardType="numeric"
                                //onFocus={Keyboard.dismiss()}
                                editable={false}
                            />
                        </Item>

                        <Item style={{ borderBottomWidth: 0, padding: 10 }}>
                            <Text style={styles.convertext2}>MYR</Text>
                            <TextInput
                                style={{ color: '#fff', width: '90%', padding: 10, fontSize: 16, fontWeight: 'bold', }}
                                value={this.state.text}
                                onChangeText={text => this.setState({ text })}
                                keyboardType="numeric"
                                //onFocus={Keyboard.dismiss()}
                                multiline={true}
                                autoCorrect={true}
                                placeholderTextColor='#6a737d'
                                placeholder={global.t('Enter_Amount')}
                            //autoFocus={true}
                            />
                        </Item>
                    </View>

                    <TouchableOpacity onPress={() => this.onSubmit()}>
                        <View style={{ backgroundColor: '#48cfb7', padding: 15, }}>
                            <Text style={styles.buttonconvertext}>{global.t('Convert')}</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={{ width: '100%', padding: 10, marginTop: 20 }}>
                        <Text style={styles.infortext}>{global.t('Total')} :</Text>
                        <Text style={{ textAlign: 'left', fontSize: 35 }}>
                            {`${currencyB.sign} `} {this.state.totalnum}
                        </Text>
                    </View>


                    <View style={{ padding: 10, marginTop: 10 }}>
                        <Text style={styles.infortext}>{global.t('Exchange_Rate')}</Text>
                        <Text style={{ marginTop: 10, }}>
                            {`${currencyA.name} (${currencyA.code}) ${currencyA.sign}${currencyA.sellRate}`} to
                                {` ${currencyB.name} (${currencyB.code}) ${currencyB.sign}${currencyB.sellRate} `}
                        </Text>
                    </View>

                </Form>

                {/* <View style={{ padding: 10, marginTop: 20 }}>
                    <Text style={styles.infortext}>{global.t('Important_Information')}</Text>
                    <View style={{ padding: 10, }}>
                        <Text style={{ marginBottom: 20 }}>
                            1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum is simply dummy text of the printing and is simply dummy text of
                            the printing and
                        </Text>
                        <Text>
                            1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum is simply dummy text of the printing and is simply dummy text of
                            the printing and
                        </Text>
                    </View>
                </View> */}

                {/* <Modal animationType={"slide"} transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>

                    <View style={{ backgroundColor: '#de2d30', padding: 30 }}>

                        <TouchableHighlight onPress={() => {
                            this.toggleModal(!this.state.modalVisible)
                        }}>
                            <Text style={styles.text}>Close Modal</Text>
                        </TouchableHighlight>
                        <Text style={{ textAlign: 'center' }}>Select Your Country</Text>
                    </View>

                    <View style={styles.modal}>
                        <FlatList
                            data={dataLocal}
                            renderItem={this.renderItem}
                        />

                    </View>
                </Modal> */}

            </View>
        );
    }
}


export default ConverterPage;

const styles = StyleSheet.create({
    degreewarp: { flexDirection: "row", padding: 20, paddingTop: 30, paddingBottom: 30 },
    degreenum: { textAlign: 'right', fontSize: 60, color: 'white' },
    degreelocation: { textAlign: 'left', fontSize: 22, color: 'white', fontWeight: 'bold' },
    degreeweather: { textAlign: 'left', fontSize: 18, color: 'white' },
    convertitle: { color: '#de2d30', fontSize: 20, fontWeight: 'bold' },
    convertext: { color: '#6ff53c', fontWeight: 'bold', fontSize: 18, textAlign: 'left' },
    convertext2: { color: '#6ff53c', fontWeight: 'bold', fontSize: 18, textAlign: 'left', width: 100 },
    converinput1: { color: 'white', fontWeight: 'bold', fontSize: 18, paddingLeft: 20 },
    converinput2: { color: 'white', fontWeight: 'bold', paddingLeft: 20, fontSize: 18 },
    buttonconvertext: { color: '#fff', textAlign: 'center', fontWeight: 'bold', fontSize: 18, },
    infortext: { color: '#de2d30', fontWeight: 'bold', fontSize: 16, },
    keycontainer: { backgroundColor: 'black' },

    text: {
        color: '#3f2949',
        marginTop: 10
    }

})
