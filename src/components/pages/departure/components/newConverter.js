import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TextInput, TouchableOpacity, Platform } from 'react-native';
import { Button, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { Select, Option } from "react-native-chooser";
import { CurrencyListApi } from '../../../../../PostApi';


class NewConverter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            resultText: "",
            calculateText: "",
            to: global.t('Please_Select_Currency'),
            data: [],
            dataConvert: [],
            base: "MYR",
        }
    }

    componentDidMount() {

        CurrencyListApi().then((fetchData) => {
            //console.log(fetchData, '--------------fetchData')
            this.setState({
                data: fetchData
            })
        })

        this.convertRate();

    }

    convertRate(base = this.state.base, to = this.state.to, value = this.state.resultText) {
        console.log(base, to, value, '-----------------------base,to,value')
        let url = `https://goldendestinations.com/api/new/currency_convert.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&base=${base}&to=${to}&value=${value}`;
        objConvert = this;
        fetch(url).then((res) => res.json())
            .then(function (myJson) {
                if (myJson[0].status == 1) {
                    objConvert.setState({
                        dataConvert: myJson[0].convert_value
                    })
                }

            })
    }

    onSelect(value) {
        this.setState({
            to: value,
        });
    }

    calculateResult() {
        const text = this.state.resultText;
        this.setState({
            calculateText: eval(text)
        })

    }

    operateremove() {
        //console.log(this.state.resultText, '----------------ops')
        let text = this.state.resultText.split('')
        text.pop()
        this.setState({
            resultText: text.join('')
        })

    }

    numPress(text) {
        //console.log(text);
        if (text == 'C') {
            return this.operateremove()
        }
        this.setState({
            resultText: this.state.resultText + text
        })

    }

    // operate(operations) {
    //     switch (operations) {
    //         case 'D':
    //             console.log(this.state.resultText, '----------------ops')
    //             let text = this.state.resultText.split('')
    //             text.pop()
    //             this.setState({
    //                 resultText: text.join('')
    //             })
    //     }
    // }


    render() {
        let rows = []
        let nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9], ['.', 0, 'C']]
        for (let i = 0; i < 4; i++) {
            let row = []
            for (let j = 0; j < 3; j++) {
                row.push(

                    <TouchableOpacity style={styles.buttonsty} onPress={() => this.numPress(nums[i][j])}>
                        <Text style={{ fontSize: 22, }}>{nums[i][j]}</Text>
                    </TouchableOpacity>

                )
            }
            rows.push(<View style={styles.rowWarp}>{row}</View>)
        }

        // let operations = ['D']
        // let ops = []
        // for (let i = 0; i < 1; i++) {
        //     ops.push(
        //         <TouchableOpacity style={styles.buttonsty} onPress={() => this.operate(operations[i])}>
        //             <Text>{operations[i]}</Text>
        //         </TouchableOpacity>
        //     )
        // }
        //console.log(this.state.resultText, '--------resultText');
        //console.log(this.state.to, '--------value');
        console.log(this.state.dataConvert, '------------------dataConvert')
        const { data, dataConvert, to, base, resultText } = this.state;
        return (
            <View >

                <Select
                    onSelect={this.onSelect.bind(this)}
                    defaultText={this.state.to}
                    style={{ borderWidth: 0, width: '100%', height: 50, backgroundColor: '#fff' }}
                    textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                    backdropStyle={{ backgroundColor: "#00000078" }}
                    optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                    animationType='none'
                    transparent={true}
                    indicator='down'
                    indicatorColor='red'
                    indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                    indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}
                >
                    {
                        data.map((item, i) => {
                            return (
                                <Option value={item.isocode} key={i}>{item.isocode} ({item.title})</Option>
                            )
                        })
                    }

                </Select>

                <View style={styles.calcWarpper}>

                    <View style={styles.inputWarp}>
                        <Text style={styles.inputTextstyle}>{base}</Text>
                        <Text style={styles.inputTextstyle2}>{resultText}</Text>
                    </View>

                    <View style={styles.inputWarp2}>
                        <Text numberOfLines={1} style={styles.inputTextstyle}>{to}</Text>
                        <Text style={styles.inputTextstyle3}>{dataConvert}</Text>
                    </View>

                    <View>{rows}</View>


                    {/* <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 3 }}>
                            {rows}
                        </View>
                        <View style={{ flex: 1 }}>
                            {ops}
                        </View>
                    </View> */}


                    <TouchableOpacity style={styles.buttonstyConvert} onPress={() => this.convertRate()}>
                        <Text style={{ color: '#fff' }}>{global.t('Convert')}</Text>
                    </TouchableOpacity>



                </View>
            </View>
        );
    }
}


export default NewConverter;

const styles = StyleSheet.create({
    calcWarpper: { flex: 1 },
    buttonsty: {
        flex: 1, height: 80, backgroundColor: '#e0e1e6', justifyContent: 'center', alignItems: 'center',
        borderRadius: 0, borderWidth: .5, borderColor: '#fff',
    },
    rowWarp: { flex: 1, flexDirection: 'row', },

    inputWarp: {
        flex: 1, flexDirection: 'row',
        height: 70, backgroundColor: '#24292e', justifyContent: 'flex-end',
    },
    inputWarp2: {
        flex: 1, flexDirection: 'row',
        height: 70, backgroundColor: '#848d95', justifyContent: 'flex-end',
    },
    inputTextstyle: { fontWeight: 'bold', fontSize: 18, color: '#fff', padding: 10, textAlign: 'left', width: '40%', marginTop: 15 },
    inputTextstyle2: { fontWeight: 'bold', fontSize: 18, color: '#6ff53c', padding: 10, textAlign: 'right', width: '60%', marginTop: 15 },
    inputTextstyle3: { fontWeight: 'bold', fontSize: 18, color: 'yellow', padding: 10, textAlign: 'right', width: '60%', marginTop: 15 },
    buttonstyConvert: {
        flex: 1, height: 70, backgroundColor: '#5fba7d', justifyContent: 'center', alignItems: 'center',
        borderRadius: 0, borderWidth: .5, borderColor: '#fff',
    },
})
