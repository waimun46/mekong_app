import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, AsyncStorage } from 'react-native';
import { Container, Header, Text, Tab, Tabs, ScrollableTab, Icon, TabHeading } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconION from "react-native-vector-icons/Ionicons";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import Note from './components/note';
import InformationPage from './components/information';
import ItineraryPage from './components/itinerary';
import HotelPage from './components/hotel';
import FlightPage from './components/flight';
import ConverterPage from './components/converter';
import NewConverter from './components/newConverter';

import { DepartingDetailApi } from '../../../../PostApi';




class DepartureInfor extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentTab: 0,
            data: [],
            AsyncStorageId: 0
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi1111')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData')
                this.setState({
                    data: fetchData
                })
            })
            // this.fetchDataApi(this.state.AsyncStorageId);
        });


    }


    render() {
        console.log(this.state.AsyncStorageId, '-----------id')
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollView>

                    <Tabs
                        renderTabBar={() => <ScrollableTab style={{ backgroundColor: 'white', }} />}
                        tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}
                        // style={{ backgroundColor: 'white', }}
                        //locked
                        initialPage={this.state.currentPage} onChangeTab={({ i }) => this.setState({ currentTab: i })}
                    >




                        {/*************** TourTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Tour')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <Note />
                        </Tab>


                        {/***************  FlightTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Flight')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <FlightPage />
                        </Tab>


                        {/***************  HotelTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Hotel')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <HotelPage />
                        </Tab>


                        {/*************** ItineraryTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Itinerary')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <ItineraryPage />
                        </Tab>


                        {/***************  ConverterTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Converter')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <NewConverter />
                        </Tab>


                        {/*************** TipsTab ***************/}
                        <Tab
                            tabStyle={styles.TabStyle}
                            heading={global.t('Tips')}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                        >
                            <InformationPage />
                        </Tab>

                    </Tabs>

                </ScrollView>

            </View>
        );
    }
}


export default DepartureInfor;

const styles = StyleSheet.create({
    IconStyle: { paddingRight: 5 },
    TabStyle: { backgroundColor: "white" },
    TabHeadingStyle: { backgroundColor: "white" },
    activeTabStyle: {
        color: 'red'
    },
    tabStyle: {
        color: 'red'
    }

})
