import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Platform, TextInput, AsyncStorage } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Toast } from 'native-base';
import { Actions } from 'react-native-router-flux';

/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const buttonText = "Okay";
const duration = 3000;
const style = { top: '15%' };


class ChangePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tokenId: 0,
            oldpass: '',
            newpass: '',
        }
    }

    updataValue(text, field) {
        console.log(text)
        if (field == 'oldpass') { this.setState({ oldpass: text }) }
        if (field == 'newpass') { this.setState({ newpass: text }) }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------ChangePassword')
            this.setState({
                tokenId: asyncStorageRes
            })
            //console.log(this.state.tokenId, '-------------ChangePassword')

            this.onSubmit(this.state.tokenId)

        });

    }

    onSubmit(access_token = this.state.tokenId, old_password = this.state.oldpass, password = this.state.newpass) {
        //console.log(access_token, '-----------------access_token')
        if (this.state.oldpass === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Old_Password'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.newpass === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_New_Password'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {

            let url = `https://goldendestinations.com/api/new/update_password.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&old_password=${old_password}&access_token=${access_token}&password=${password}`;

            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    //console.log(myJson, '-----------------myJson')
                    if (myJson[0].status == 1) {
                        return myJson.json(Actions.Home());
                    }
                    else {
                        alert(myJson[0].error)
                    }
                })

        }

    }

    render() {
        return (

            <View style={styles.HotpickContainer}>
                <ScrollView>
                    <Content>
                        <Form>
                            <Item stackedLabel>
                                <Label>{global.t('Fill_Old_Password')}</Label>
                                <TextInput
                                    value={this.state.oldpass}
                                    // placeholder='oldpass'
                                    // placeholderTextColor="#000"
                                    underlineColorAndroid='transparent'
                                    style={[styles.TextInputStyle,
                                    { backgroundColor: this.state.TextInputDisableStatus ? '#FFF' : '#FFF' }]}
                                    editable={this.state.TextInputDisableHolder}
                                    ref="thisTextInput"
                                    autoCorrect={false}
                                    secureTextEntry
                                    autoCapitalize={false}
                                    onChangeText={(text) => this.updataValue(text, 'oldpass')}
                                    editable={this.state.isEditable}
                                />
                            </Item>
                            <Item stackedLabel last>
                                <Label>{global.t('Fill_New_Password')}</Label>
                                <TextInput
                                    value={this.state.newpass}
                                    // placeholder='newpass'
                                    // placeholderTextColor="#000"
                                    underlineColorAndroid='transparent'
                                    style={[styles.TextInputStyle,
                                    { backgroundColor: this.state.TextInputDisableStatus ? '#FFF' : '#FFF' }]}
                                    editable={this.state.TextInputDisableHolder}
                                    ref="thisTextInput"
                                    autoCorrect={false}
                                    secureTextEntry
                                    autoCapitalize={false}
                                    onChangeText={(text) => this.updataValue(text, 'newpass')}
                                    editable={this.state.isEditable}
                                />
                            </Item>
                        </Form>
                    </Content>
                </ScrollView>
                {/*************************** Submit button ***************************/}
                <View style={styles.Buttonwarp}>
                    <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                        <Text style={styles.BottomText} uppercase={false}>{global.t('Submit')}</Text>
                    </Button>
                </View>
            </View>
        );
    }
}


export default ChangePassword;

const styles = StyleSheet.create({
    HotpickContainer: { backgroundColor: 'white', flex: 1 },
    Buttonwarp: {
        position: 'absolute', flex: 0.1, left: 0, right: 0, bottom: -20, flexDirection: 'row', height: 80,
        alignItems: 'center', width: '100%',
    },
    Buttonwarp: {
        position: 'absolute', bottom: 0, flex: 0.1, left: 0, right: 0,
        alignItems: 'center', width: '100%',
    },
    bottomStyle: {
        width: '100%',
        backgroundColor: '#fc564e', borderRadius: 0,
        ...Platform.select({ 
            ios: { height: 70 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16 },
    TextInputStyle: { textAlign: 'left', height: 40, marginBottom: 0, marginTop: 10, width: '80%', marginRight: 'auto' },
})
