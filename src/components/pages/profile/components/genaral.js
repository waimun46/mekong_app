import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TextInput, TouchableOpacity, AsyncStorage, Platform ,SafeAreaView} from 'react-native';
import { Form, Item, Input, Label, Text, Button, Toast, ListItem, Icon, Left, Body, Right, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import RNLanguages from 'react-native-languages';
import i18n from '../../../../lang/index';
import en from '../../../../lang/locales/en.json';
import zh from '../../../../lang/locales/zh.json';
import RNRestart from "react-native-restart"
import { UserProfileApi } from '../../../../../PostApi';



/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const buttonText = "Okay";
const duration = 3000;
const style = { top: '15%' };



class Genaral extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // access_token: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            city: '',
            postcode: '',
            state: '',
            TextInputDisableStatus: true,
            PassInputDisableStatus: true,
            TextPhoneDisableStatus: true,
            AdsInputDisableStatus: true,
            CityInputDisableStatus: true,
            PostInputDisableStatus: true,
            StateInputDisableStatus: true,
            Error: '',
            tokenId: 0,
            data: [],
            isEditable: false,
        }

    }
    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage')
            this.setState({
                tokenId: asyncStorageRes
            })
            //console.log(this.state.tokenId, '-------------fetchDataApi1111')

            /************************************ Fetch Api Data  ***********************************/
            UserProfileApi(this.state.tokenId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData')
                this.setState({
                    data: fetchData[0]
                })
            })

            // this.onSubmit(this.state.tokenId)

        });

    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'email') { this.setState({ TextInputDisableStatus: false, email: this.refs.thisTextInput.value, email: text }) }
        if (field == 'access_token') { this.setState({ TextInputDisableStatus: false, access_token: this.refs.thisTextInput.value, access_token: text }) }
        if (field == 'password') { this.setState({ PassInputDisableStatus: false, password: this.refs.thisTextInput.value, password: text }) }
        if (field == 'phone') { this.setState({ TextPhoneDisableStatus: false, phone: this.refs.thisNumInput.value, phone: text }) }
        if (field == 'address') { this.setState({ AdsInputDisableStatus: false, address: this.refs.thisAdsInput.value, address: text }) }
        if (field == 'city') { this.setState({ CityInputDisableStatus: false, city: this.refs.thiscityInput.value, city: text }) }
        if (field == 'postcode') { this.setState({ PostInputDisableStatus: false, postcode: this.refs.thispostInput.value, postcode: text }) }
        if (field == 'state') { this.setState({ StateInputDisableStatus: false, state: this.refs.thisstateInput.value, state: text }) }
    }

    /****************************************** Change Language ******************************************/
    change = async (language) => {
        console.info("== changing language to:" + language)
        await AsyncStorage.setItem("language", language)
        RNRestart.Restart()
    }
    // state = {
    //     currentLanguage: RNLanguages.language
    // };

    // _changeLanguage = (language) => {
    //     this.setState({ currentLanguage: language });
    // };

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(tokenId = this.state.tokenId, password = this.state.password, phone = this.state.phone,
        address = this.state.address, city = this.state.city, postcode = this.state.postcode, state = this.state.state) {
        console.log(tokenId, 'onSubmit------------------fetch------------tokenId')

        /****************************** Validations form Toast *****************************/
        // if (this.state.access_token === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: 'Please Fill In The Access Token',
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // if (this.state.email === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Email'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        if (this.state.password === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Password'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.phone === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Phone'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.address === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Address'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.city === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_City'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.postcode === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Postcode'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.state === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_State'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {
            this.setState({ Error: '' })
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/update_profile.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${tokenId}&phone=${phone}&password=${password}&city=${city}&address=${address}&postcode=${postcode}&state=${state}`;

            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    //console.log(myJson, '-----------myJson');
                    if (myJson[0].status === 1) {
                        return myJson.json(Actions.Home());
                    }
                    else {
                        alert(myJson[0].error)
                    }
                })

        }
    }

    render() {
        const { Error, data, isEditable, email, phone, address, city, state, postcode } = this.state;
        console.log(this.state.tokenId, '----------------tokenId')
        return (
            <View style={{ width: '100%', height: '100%',backgroundColor: '#fff', }}>
                <View style={{ flex: 1, }}>
                    <ScrollView style={{ width: '100%', }}>
                        <View style={{ flex: .8, }}>
                            <Form >
                                {/********************************** Genaral **********************************/}
                                <View style={styles.GenaralWarp}>
                                    {/* <Text style={styles.errormsg}>{Error}</Text> */}
                                    <View style={styles.TopTextContainer}>
                                        <View style={{ width: '50%', paddingLeft: 15 }}>
                                            <Text style={styles.TopTextTitle}>
                                                {global.t('General')}
                                            </Text>
                                        </View>
                                    </View>
                                    {/******************* Email  ********************/}
                                    <Item stackedLabel style={{ marginBottom: 10, borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Email')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={email}
                                                placeholder={data.email}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.TextInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisTextInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'email')}
                                                editable={isEditable}
                                            />
                                            {/* <IconANT
                                        name="form"
                                        onPress={this.onPressButton}
                                        size={20}
                                        style={{ color: this.state.TextInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                    /> */}
                                        </View>
                                    </Item>
                                    {/***************** Password  ********************/}
                                    <Item stackedLabel style={{ borderBottomWidth: 0 }}>
                                        <ListItem icon style={styles.PasswordListItam} onPress={() => Actions.changepassword()}>
                                            <Body style={{ borderBottomWidth: 0 }}>
                                                <Text style={styles.passStyle}>{global.t('Change_Password')}</Text>
                                            </Body>
                                            <Right style={{ borderBottomWidth: 0 }}>
                                                <Icon active name="arrow-forward" />
                                            </Right>
                                        </ListItem>


                                        {/* <Label style={styles.LabelStyle}>{global.t('Password')}</Label>
                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                    <TextInput
                                        value={this.state.password}
                                        underlineColorAndroid='transparent'
                                        style={[styles.TextInputStyle,
                                        { backgroundColor: this.state.PassInputDisableStatus ? '#FFF' : '#FFF' }]}
                                        editable={this.state.TextInputDisableHolder}
                                        ref="thisTextInput"
                                        secureTextEntry
                                        autoCorrect={false}
                                        autoCapitalize={false}
                                        onChangeText={(text) => this.updataValue(text, 'password')}
                                    />
                                    <IconANT
                                        name="form"
                                        onPress={this.onPressPassButton}
                                        size={20}
                                        style={{ color: this.state.PassInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                    />
                                </View> */}
                                    </Item>
                                    {/******************* Change language  ******************
                            <Item stackedLabel style={styles.langwarpContainer}>
                                <View style={{ width: '65%' }}>
                                    <Label style={styles.LabelStylelang}>{global.t('Change_language')}</Label>
                                </View>
                                <View style={{ width: '35%', }}>
                                    <View style={styles.langwarp}>
                                        <TouchableOpacity onPress={() => this.change('zh')} style={{ marginRight: 15, textAlign: 'right', }} >
                                            <Image source={require('../../../../assets/images/icon/cn.png')} style={styles.langicon} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.change('en')} style={{ textAlign: 'right', }}>
                                            <Image source={require('../../../../assets/images/icon/en.png')} style={styles.langicon} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Item>
                            */}
                                </View>

                                {/************************************* Personal *************************************/}
                                <View style={styles.BottomContainer} >
                                    <View style={styles.TopTextContainer}>
                                        <View style={{ width: '50%', paddingLeft: 15 }}>
                                            <Text style={styles.TopTextTitle}>{global.t('Personal')}</Text>
                                        </View>
                                    </View>
                                    {/******************* Phone  *******************/}
                                    <Item stackedLabel style={{ marginBottom: 10, borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Phone')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={phone}
                                                placeholder={data.phone}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.TextPhoneDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisNumInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'phone')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressPhoneButton}
                                                size={20}
                                                style={{ color: this.state.TextPhoneDisableStatus ? '#d1d5da' : '#de2d30' }}
                                            />
                                        </View>
                                    </Item>
                                    {/******************* Address  *******************/}
                                    <Item stackedLabel style={{ borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Address')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={address}
                                                placeholder={data.address}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisAdsInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'address')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressAdsButton}
                                                size={20}
                                                style={{ color: this.state.AdsInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                            />
                                        </View>
                                    </Item>
                                    <View style={styles.AddressContainer}>
                                        {/******************* City  *******************/}
                                        <Item stackedLabel style={styles.AddressItem}>
                                            <Label style={styles.LabelStyle}>{global.t('City')}</Label>
                                            <TextInput
                                                value={city}
                                                placeholder={data.city}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thiscityInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'city')}
                                            />
                                        </Item>
                                        {/******************* Postcode  *******************/}
                                        <Item stackedLabel style={styles.AddressItem}>
                                            <Label style={styles.LabelStyle}>{global.t('Postcode')}</Label>
                                            <TextInput
                                                value={postcode}
                                                placeholder={data.postcode}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thispostInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'postcode')}
                                            />
                                        </Item>
                                    </View>
                                    {/******************* State  *******************/}
                                    <Item stackedLabel style={{ marginTop: 10, borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('State')}</Label>
                                        <TextInput
                                            value={state}
                                            placeholder={data.state}
                                            placeholderTextColor="#000"
                                            underlineColorAndroid='transparent'
                                            style={[styles.TextInputStyle,
                                            { backgroundColor: this.state.AdsInputDisableStatus ? '#fff' : '#fff' }]}
                                            editable={this.state.TextInputDisableHolder}
                                            ref="thisstateInput"
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                            onChangeText={(text) => this.updataValue(text, 'state')}
                                        />
                                    </Item>
                                </View>
                            </Form>
                        </View>
                    </ScrollView>
                    {/*************************** Submit button ***************************/}
                    <View style={styles.Buttonwarp}>
                        <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                            <Text style={styles.BottomText} uppercase={false}>{global.t('Submit')}</Text>
                        </Button>
                    </View>
                    {/* <SafeAreaView style={{ flex: 0, backgroundColor: '#fc564e' }} />  */}
                </View>
            </View>

        );
    }
}


export default Genaral;


const styles = StyleSheet.create({
    TopTextContainer: { flexDirection: 'row', width: '100%', },
    TopTextTitle: { fontSize: 20, color: '#de2d30', fontWeight: 'bold', marginBottom: 10 },
    TopIconContainer: { width: '50%', alignItems: 'flex-end', },
    TopIconStyle: { color: '#de2d30', },
    LabelStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', },
    passStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', fontSize: 15 },
    InputStyle: { color: 'black', },
    BottomContainer: {  paddingTop: 20, paddingLeft: 20, paddingRight: 30, paddingBottom: 100 },
    AddressContainer: { flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between', },
    AddressItem: { width: '40%', marginTop: 10, borderColor: '#959da5' },
    TextInputStyle: { textAlign: 'left', height: 40, marginBottom: 0, marginTop: 10, width: '80%', marginRight: 'auto' },
    langwarpContainer: { marginTop: 10, flexDirection: 'row', borderBottomWidth: 0 },
    langwarp: { width: '100%', marginLeft: 'auto', flexDirection: 'row', justifyContent: 'flex-end', },
    langicon: { width: 50, height: 30, },
    subbtnWarp: {
        width: '70%', backgroundColor: '#de2d30', padding: 15, marginTop: 50, marginRight: 'auto',
        marginLeft: 'auto', borderRadius: 50
    },
    LabelStylelang: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', marginTop: -20 },
    Buttonwarp: {
        position: 'absolute', bottom: 0, flex: 0.1, left: 0, right: 0,
        alignItems: 'center', width: '100%',
    },
    bottomStyle: {
        width: '100%',
        backgroundColor: '#fc564e', borderRadius: 0,
        ...Platform.select({ 
            ios: { height: 70 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16, color: '#fff' },
    errormsg: { textAlign: 'right', width: '100%', position: 'absolute', right: 0, color: 'blue', padding: 10 },
    PasswordListItam: { width: '100%', paddingTop: 10, marginLeft: 0, borderBottomWidth: 0 },
    GenaralWarp: { backgroundColor: '#e9ecef', paddingLeft: 20, paddingRight: 30, paddingTop: 20, }

})
