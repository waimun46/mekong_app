import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, FlatList, TouchableOpacity, Platform, Image, TextInput, AsyncStorage } from 'react-native';
import { Form, Item, Input, Label, Button, Toast } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import Modal from "react-native-modal";


class LoginModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: true,
            email: '',
            username: '',
            password: '',
            Error: '',
            data: [],
            access_token: 0,
            tokenId: 0
        }
        this._closeModal = this._closeModal.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    _closeModal = () => {
        this.setState({ isModalVisible: false });
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        //if (field == 'username') { this.setState({ username: text }) }
        if (field == 'password') { this.setState({ password: text }) }
        if (field == 'email') { this.setState({ email: text }) }
    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(password = this.state.password, email = this.state.email) {
        console.log( password, '----------------username,password')

        /****************************** Validations form Toast *****************************/
        // if (this.state.username === "") {
        //     this.setState({ Error: global.t('Fill_Username') })
        // }
        if (this.state.email === "") {
            this.setState({ Error: global.t('Fill_Email') })
        }
        else if (this.state.password === "") {
            this.setState({ Error: global.t('Fill_Password') })
        }
        else {
            this.setState({ Error: '' })

            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/login.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&email=${email}&password=${password}`;
            obj_setState = this;
            fetch(url)
                .then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, 'myJson-----------');
                    if (myJson[0].status == 1) {
                        console.log(obj_setState.state);

                        /****************** Store Data AsyncStorage ******************/
                        AsyncStorage.setItem('USER_TOKEN', myJson[0].access_token);
                        obj_setState.setState({
                            isModalVisible: false,
                            access_token: myJson[0].access_token

                        });

                    } else {
                        alert(myJson[0].error);
                    }
                });
        }
    }


    render() {
        const { Error } = this.state;
        console.log(this.state.access_token, 'access_token-------login')
        return (
            <View style={{ flex: 1 }}>

                <Modal isVisible={this.state.isModalVisible} >
                    <ScrollView>
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity onPress={this._toggleModal}>
                                <IconANT name="close" size={30} style={{ color: '#fff', textAlign: 'right', marginTop: Platform.OS === 'ios' ? 20 : 0 }} />
                            </TouchableOpacity>

                            <View style={styles.contentWarp}>
                                <Form >
                                    <View style={styles.warpperstyle}>
                                        <View>
                                            <Image
                                                source={require('../../../assets/images/logo_r.png')}
                                                style={styles.logoImage}
                                            />
                                        </View>

                                        {/*************************** Username  **************************
                                        <Item style={{ borderColor: 'white', marginLeft: 0, marginTop: 20 }}>
                                            <Label style={styles.labelstyle}>{global.t('Username')}</Label>
                                            <TextInput
                                                placeholder={global.t('Placeholder_Fill_Username')}
                                                placeholderTextColor='#ffffffad'
                                                style={styles.inputstyle}
                                                onChangeText={(text) => this.updataValue(text, 'username')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            />
                                        </Item>*/}

                                        {/*************************** Email  ***************************/}
                                        <Item style={{ borderColor: 'white', marginLeft: 0, marginTop: 15 }}>
                                            <Label style={styles.labelstyle}>{global.t('Email')}</Label>
                                            <TextInput
                                                placeholder={global.t('Placeholder_Fill_Email')}
                                                placeholderTextColor='#ffffffad'
                                                style={styles.inputstyle}
                                                onChangeText={(text) => this.updataValue(text, 'email')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            />
                                        </Item>
                                        {/*************************** Password  ***************************/}
                                        <Item style={{ marginTop: 15, marginBottom: 15, borderColor: 'white', marginLeft: 0 }}>
                                            <Label style={styles.labelstyle}>{global.t('Password')}</Label>
                                            <TextInput
                                                placeholder={global.t('Placeholder_Fill_Password')}
                                                placeholderTextColor='#ffffffad'
                                                secureTextEntry={true}
                                                style={styles.inputstyle}
                                                onChangeText={(text) => this.updataValue(text, 'password')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            />
                                        </Item>

                                        {/*************************** Error message  ***************************/}
                                        <Item style={{ width: '100%', position: 'absolute', bottom: 0, borderBottomWidth: 0 }}>
                                            <View style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                                                <Text style={styles.errormsg}>{Error}</Text>
                                            </View>
                                        </Item>


                                    </View>
                                    {/*************************** Submit button ***************************/}
                                    <View style={styles.bottomwrap}>
                                        <View style={styles.contentwarp}>
                                            <Text style={{ textAlign: 'center', fontSize: 15, lineHeight: 20 }}>{global.t('loginwel')}</Text>
                                        </View>

                                        <View style={styles.btnwarp}>
                                            {/******************* Sign up button ******************/}
                                            <Button transparent style={styles.btnregister}
                                                onPress={() => { Actions.register(); this._toggleModal(); }}
                                            >
                                                <Text style={{ color: '#6a737c', fontSize: 16 }}>{global.t('Sign_up')}</Text>
                                            </Button>
                                            {/******************* Login button ******************/}
                                            <Button transparent style={styles.btnlogin} onPress={() => this.onSubmit()}>
                                                <Text style={{ color: '#0095ff', fontSize: 16 }}>{global.t('Login')}</Text>
                                            </Button>
                                        </View>
                                    </View>
                                </Form>
                            </View>

                        </View>
                    </ScrollView>
                </Modal>

            </View>
        );
    }

}


export default LoginModal;

const styles = StyleSheet.create({
    contentWarp: {
        width: '95%', marginLeft: 'auto', marginRight: 'auto',
        ...Platform.select({
            ios: { marginTop: 120, },
            android: { marginTop: 80, }
        })
    },
    logoImage: { resizeMode: 'contain', width: '100%', height: 80 },
    labelstyle: { color: '#fff', width: '30%', paddingTop: 5, paddingBottom: 5, fontSize: 14 },
    inputstyle: { color: 'white', paddingLeft: 20, width: '70%', height: 55, },
    warpperstyle: {
        width: '100%', backgroundColor: '#24292e',
        borderTopLeftRadius: 10, borderTopRightRadius: 10, padding: 30, marginTop: -50, width: '100%'
    },
    bottomwrap: { width: '100%', backgroundColor: 'white', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, },
    contentwarp: { padding: 20, paddingTop: 20, paddingBottom: 20 },
    btnwarp: { flexDirection: 'row', borderTopWidth: 0.5, borderColor: '#ccc', padding: 10, },
    btnregister: { width: '50%', justifyContent: "center", borderRightWidth: 0.5, borderColor: '#ccc' },
    btnlogin: { width: '50%', justifyContent: "center", },
    errormsg: { textAlign: 'center', color: 'yellow', marginLeft: 20, padding: 10 },
})
