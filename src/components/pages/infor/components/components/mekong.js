import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Image,  } from 'react-native';
import { Text, List, ListItem, Body,  Button, Segment, Content, } from 'native-base';
import { mekong } from '../data';

{/***********************************  inbound Component  **********************************/ }
class FirstComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataIndound: mekong[0].inbound,
        }
    }
    render() {
        const { dataIndound } = this.state;
        //console.log(dataIndound, 'mekong')
        const data = dataIndound.map((item, i) => {
            return (
                <List style={{ backgroundColor: '#fff', marginBottom: 10, }} key={i}>
                    <ListItem style={{ borderBottomWidth: 0 }}>
                        <Body>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Company_Name')} :</Text>
                                <Text note style={styles.contenttext}>{item.company_name}</Text>
                            </View>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Destinations')} :</Text>
                                <Text note style={styles.contenttext}>{item.destinations}</Text>
                            </View>

                            <View style={styles.contentWarp}>
                                <Text note style={styles.titlecontent}>{global.t('Group_Size')} :</Text>
                                <Text note style={styles.contenttext}>{item.group_size}</Text>
                            </View>
                        </Body>

                    </ListItem>
                </List>
            )
        })
        return (
            <View>
                {data}
            </View>

        );
    }
}

{/*********************************  outbound Component  *********************************/ }
class SecondComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOutbound: mekong[0].outbound
        }
    }
    render() {
        const { dataOutbound } = this.state;
        //console.log(dataOutbound, 'mekong')
        const data = dataOutbound.map((item, i) => {
            return (
                <List style={{ backgroundColor: '#fff', marginBottom: 10, }} key={i}>
                    <ListItem style={{ borderBottomWidth: 0 }}>
                        <Body>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Company_Name')} :</Text>
                                <Text note style={styles.contenttext}>{item.company_name}</Text>
                            </View>
                            <View style={[styles.contentWarp, { marginBottom: 5 }]}>
                                <Text note style={styles.titlecontent}>{global.t('Destinations')} :</Text>
                                <Text note style={styles.contenttext}>{item.destinations}</Text>
                            </View>

                            <View style={styles.contentWarp}>
                                <Text note style={styles.titlecontent}>{global.t('Group_Size')} :</Text>
                                <Text note style={styles.contenttext}>{item.group_size}</Text>
                            </View>
                        </Body>

                    </ListItem>
                </List>
            )
        })
        return (
            <View>
                {data}
            </View>
        );
    }
}



class Mekong extends Component {

    constructor(props) {
        super(props)
        this.state = {
            activePage: 1,
        }
    }

    selectComponent = (activePage) => () => this.setState({ activePage })

    _renderComponents = () => {
        if (this.state.activePage === 1) {
            return <FirstComponent />
        } else {
            return <SecondComponent />
        }
    }

    render() {

        const { activePage } = this.state;

        return (
            <View style={{ flex: 1, }}>
                <ScrollView>
                    <View style={{ backgroundColor: '#fff' }}>
                        <Image source={require('../../../../../assets/images/mklogo.jpg')}
                            style={{ resizeMode: 'contain', width: '100%', height: 150 }}
                        />
                    </View>

                    <View>
                        <Body style={styles.bodyWarp}>
                            <Segment style={{ backgroundColor: 'transperant', borderRadius: 0, }}>
                                <Button style={[styles.btnsgStyle, {
                                    backgroundColor: activePage === 1 ? '#de2d30' : '#e9e9ef',
                                    borderColor: activePage === 1 ? '#de2d30' : '#de2d30',
                                }]}
                                    first active={activePage === 1} onPress={this.selectComponent(1)}>
                                    <Text style={[styles.btntitle, { color: activePage === 1 ? '#fff' : '#de2d30' }]}>
                                        {global.t('INBOUND')}
                                    </Text>
                                </Button>
                                <Button style={[styles.btnsgStyle, {
                                    backgroundColor: activePage === 2 ? '#de2d30' : '#e9e9ef',
                                    borderColor: activePage === 2 ? '#de2d30' : '#de2d30',
                                }]}
                                    last active={activePage === 2} onPress={this.selectComponent(2)}>
                                    <Text style={[styles.btntitle, { color: activePage === 2 ? '#fff' : '#de2d30' }]}>
                                        {global.t('OUTBOUND')}
                                    </Text>
                                </Button>
                            </Segment>
                        </Body>

                        <Content padder>
                            {this._renderComponents()}
                        </Content>
                    </View>

                </ScrollView>
            </View>

        );
    }
}



export default Mekong;

const styles = StyleSheet.create({
    btnsgStyle: {
        width: '50%', textAlign: 'center', alignItems: 'center',
        borderTopLeftRadius: 0, borderBottomLeftRadius: 0,
        borderTopRightRadius: 0, borderBottomRightRadius: 0, height: 40
    },
    bodyWarp: { width: '100%', padding: 10, paddingBottom: 0 },
    btntitle: { marginLeft: 'auto', marginRight: 'auto', color: '#de2d30' },
    titlecontent: { width: '40%', marginLeft: 0, marginRight: 0, textAlign: 'left', },
    contenttext: { textAlign: 'right', width: '60%', marginLeft: 0, marginRight: 0, fontWeight: 'bold' },
    contentWarp: { flexDirection: 'row', },



})