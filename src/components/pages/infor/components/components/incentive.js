import React, { Component } from 'react';
import { AsyncStorage, Image, ScrollView, StyleSheet, Text, View, FlatList } from 'react-native';
import { Content, Icon, Accordion } from 'native-base';
import IconANT from "react-native-vector-icons/AntDesign";
import { dataArray_incentive } from '../data';

class IncentiveGroup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataArray: dataArray_incentive,
            langPrint: ''
        }
    }

    componentDidMount() {
        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----InformationPage')
            this.setState({
                langPrint: langStorageRes
            })
        })
    }


    render() {
        //console.log(this.state.dataArray, 'dataArray_incentive')
        const { dataArray, langPrint } = this.state;
        return (
            <View style={{ backgroundColor: '#fff', flex: 1, marginBottom: 30 }}>
                <ScrollView>

                    <View>
                        <View style={styles.topimgwarp}>
                            <Image source={require('../../../../../assets/images/img/ww.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('Incentive_Group')}</Text>
                            </View>
                        </View>

                        <View style={[styles.boxwarp, { marginTop: 20 }]}>
                            <Text style={styles.contenttext}>
                                {global.t('globalization')}
                            </Text>
                            <Text style={[styles.contenttext, { paddingTop: 0 }]}>
                                {global.t('Mekong_Discovery')}
                            </Text>
                        </View>

                        {/************************************ Accordion dataArray_incentive ***********************************/}

                        <Content padder style={{ paddingBottom: 30 }}>
                            <Accordion
                                dataArray={dataArray}
                                animation={true}
                                expanded={true}
                                style={{ borderWidth: 0 }}
                                renderHeader={(item, expanded) => (
                                    <View style={styles.renderHeaderWarp}>
                                        <Text style={{ fontWeight: "600" }}>{" "}
                                            {langPrint === 'zh' ? (item.title_cn) : (item.title_en)}
                                        </Text>
                                        {expanded
                                            ? <Icon style={{ fontSize: 18 }} name="remove" />
                                            : <Icon style={{ fontSize: 18 }} name="add" />}
                                    </View>
                                )}
                                renderContent={(item) => (
                                    <View style={styles.renderContentWarp}>
                                        <View style={{ marginBottom: 5 }}>
                                            <Text style={{ lineHeight: 20, color: '#de2d30', }}>
                                                {langPrint === 'zh' ? (item.content_cn) : (item.content_en)}
                                            </Text>
                                        </View>
                                    </View >
                                )}
                            />
                        </Content>
                    </View>

                </ScrollView>
            </View>
        );
    }
}



export default IncentiveGroup;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 26, textAlign: 'center' },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'left', marginTop: 10, paddingBottom: 10, padding: 20, fontSize: 23, color: '#de2d30', fontWeight: 'bold' },
    contenttitle: { textAlign: 'center', padding: 10, color: '#000', fontWeight: 'bold', fontSize: 20, },
    contenttext: { textAlign: 'center', padding: 25, paddingBottom: 10, color: '#777', lineHeight: 20, paddingTop: 5 },
    contenttext2: { textAlign: 'center', padding: 20, marginTop: -10, color: '#777', },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black', width: '100%' },
    boxwarp: { paddingBottom: 5, },
    contenTitleStyle: { fontWeight: 'bold', marginBottom: 5, color: '#de2d30', },
    renderHeaderWarp: {
        flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center",
        backgroundColor: "#ececec", marginBottom: 5,
    },
    renderContentWarp: { backgroundColor: "#fcf5f5", padding: 10, marginBottom: 10, marginTop: -3, },
    iconstyle: { fontSize: 15, paddingRight: 10, },
    textWarpcontent: { flexDirection: 'row', marginBottom: 10 }



})