import React, { Component } from 'react';
import { AsyncStorage, Image, ScrollView, StyleSheet, Text, View, FlatList } from 'react-native';
import IconANT from "react-native-vector-icons/AntDesign";

class MiceSpecial extends Component {

    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>

                    <View>
                        <View style={styles.topimgwarp}>
                            <Image source={require('../../../../../assets/images/img/sp.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('mice_special')}</Text>
                            </View>
                        </View>



                        {/*********************************** Meetings *********************************** */}
                        <View style={styles.boxwarp}>
                            <Text style={styles.subtitle}>{global.t('Meetings')}</Text>
                            <View style={{ padding: 10 }}>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Knowledge_market')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Venue_sourcing')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Delegates_registration')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Staffing')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('equipment_delivery')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Various_social')}</Text>
                                </View>
                            </View>
                        </View>

                        {/*********************************** Incentive *********************************** */}
                        <View style={styles.boxwarp}>
                            <Text style={[styles.subtitle, { marginTop: 0, paddingTop: 10, }]}>{global.t('Incentive')}</Text>
                            <View style={{ padding: 10 }}>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Accommodation')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Careful_choice')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Entertainment')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Implementation')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Budget')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Logistic')}</Text>
                                </View>
                            </View>
                        </View>

                        {/*********************************** Conference *********************************** */}
                        <View style={styles.boxwarp}>
                            <Text style={[styles.subtitle, { marginTop: 0, paddingTop: 10 }]}>{global.t('Conference')}</Text>
                            <View style={{ padding: 10 }}>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Conference_material')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Logistic')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Delegates_registration')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Staffing')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('equipment_delivery')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Various_social')}</Text>
                                </View>
                            </View>
                        </View>

                        {/*********************************** Exhibition ************************************/}
                        <View style={styles.boxwarp}>
                            <Text style={[styles.subtitle, { marginTop: 0, paddingTop: 10 }]}>{global.t('Exhibition')}</Text>
                            <View style={{ padding: 10 }}>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('New_product')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Get_sales')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Gather_information')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Build_networks')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Building_image')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Create_excellent')}</Text>
                                </View>
                            </View>
                        </View>

                        {/*********************************** Team_Building ************************************/}
                        <View style={styles.boxwarp}>
                            <Text style={[styles.subtitle, { marginTop: 0, paddingTop: 10, paddingBottom: 0 }]}>{global.t('Team_Building')}</Text>
                            <View style={{ padding: 10 }}>
                                <View style={[styles.textWarpcontent, { paddingRight: 10, paddingLeft: 10, paddingTop: 5, paddingBottom: 5 }]}>
                                    <Text style={{ lineHeight: 20 }}>{global.t('Team_Building_Content')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <Text style={{ color: '#de2d30', fontWeight: 'bold', }}>{global.t('memento')} :</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Perfect_develop')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Great_source')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Develop_adaptation')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Treasure_hunting')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Great_tools')}</Text>
                                </View>
                                <View style={styles.textWarpcontent}>
                                    <IconANT name="star" style={styles.iconstyle} />
                                    <Text style={styles.textstyInner}>{global.t('Develop_planning')}</Text>
                                </View>
                                <View style={[styles.textWarpcontent, { paddingRight: 10, paddingLeft: 10 }]}>
                                    <Text style={{ lineHeight: 20 }}>{global.t('Depending_concept')}</Text>
                                </View>
                            </View>
                        </View>


                    </View>

                </ScrollView>
            </View>
        );
    }
}



export default MiceSpecial;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 26, textAlign: 'center' },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'left', marginTop: 10, paddingBottom: 10, padding: 20, fontSize: 23, color: '#de2d30', fontWeight: 'bold' },
    contenttitle: { textAlign: 'center', padding: 10, color: '#000', fontWeight: 'bold', fontSize: 20, },
    contenttext: { textAlign: 'center', padding: 25, paddingBottom: 10, color: '#777', lineHeight: 20, paddingTop: 5 },
    contenttext2: { textAlign: 'center', padding: 20, marginTop: -10, color: '#777', },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black', width: '100%' },
    boxwarp: { paddingBottom: 5, },
    contenTitleStyle: { fontWeight: 'bold', marginBottom: 5, color: '#de2d30', },
    renderHeaderWarp: {
        flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center",
        backgroundColor: "#ececec", marginBottom: 5,
    },
    renderContentWarp: { backgroundColor: "#fcf5f5", padding: 10, marginBottom: 10, marginTop: -3, },
    iconstyle: { fontSize: 15, width: '9%', textAlign: 'center' },
    textWarpcontent: { flexDirection: 'row', marginBottom: 10, paddingRight: 10 },
    textstyInner: { width: '95%' }



})