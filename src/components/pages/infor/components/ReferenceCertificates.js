import React, { Component } from 'react';
import { AsyncStorage, Image, ScrollView, StyleSheet, Text, View, FlatList } from 'react-native';
import { reference, certificate, partner } from './data';

const numColumns = 4;
const numColumnsCer = 3;


class Reference extends Component {

    /****************************** logo flatlist render ******************************/
    renderItem = ({ item }) => {
        //console.log(item)
        return (
            <View style={{ flex: 1, padding: 5 }}>
                <View style={styles.renderImageWarp}>
                    <Image source={item.logo} style={styles.ImgWrp} />
                </View>
            </View>
        )
    }

    /****************************** cer flatlist render ******************************/
    _renderItem = ({ item }) => {
        //console.log(item)
        return (
            <View style={{ flex: 1, padding: 5 }}>
                <View style={styles.renderImageWarp}>
                    <Image source={item.cer} style={styles.ImgWrp} />
                </View>
            </View>
        )
    }


    render() {

        //console.log(partner[0].gsa, 'dataPartner')

        return (
            <View style={{ flex: 1, backgroundColor: '#fff', }}>
                <ScrollView>

                    {/**********************************  logo **********************************/}
                    <View>
                        <View style={[styles.topimgwarp, { marginBottom: 10 }]}>
                            <Image source={require('../../../../assets/images/img/ref.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('Our_Reference')}</Text>
                            </View>
                        </View>
                        <View style={{ padding: 10 }}>
                            <FlatList
                                data={reference}
                                renderItem={this.renderItem}
                                numColumns={numColumns}
                                keyExtractor={(item, index) => item.key}
                            />
                        </View>
                    </View>

                    {/**********************************  cer **********************************/}
                    <View style={{ marginTop: 20, paddingBottom: 10, }}>
                        <View style={[styles.topimgwarp]}>
                            <Image source={require('../../../../assets/images/img/trp.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('Our_Certificates')}</Text>
                            </View>
                        </View>

                        <View style={{ padding: 10, marginTop: 20 }}>
                            <FlatList
                                data={certificate}
                                renderItem={this._renderItem}
                                numColumns={numColumnsCer}
                                keyExtractor={(item, index) => item.id}
                            />
                        </View>
                    </View>


                    {/**********************************  partner **********************************/}
                    <View style={{ marginTop: 20, paddingBottom: 30 }}>
                        <View style={[styles.topimgwarp]}>
                            <Image source={require('../../../../assets/images/img/partner.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('OUR_PARTNER')}</Text>
                            </View>
                        </View>

                        <View style={{ padding: 10, marginTop: 20 }}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <View>
                                    <View>
                                        <Text style={styles.partnertitlesty}>GSA</Text>
                                        <View style={styles.borderSty}><Text></Text></View>
                                    </View>
                                    <View style={styles.imgwarpsty}>
                                        <Image source={partner[0].gsa[0].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                        <Image source={partner[0].gsa[1].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                        <Image source={partner[0].gsa[2].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                    </View>
                                </View>
                                <View style={{ marginTop: 40 }}>
                                    <View>
                                        <Text style={styles.partnertitlesty}>PSA</Text>
                                        <View style={styles.borderSty}><Text></Text></View>
                                    </View>
                                    <View style={styles.imgwarpsty}>
                                        <Image source={partner[0].psa[0].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                        <Image source={partner[0].psa[1].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                        <Image source={partner[0].psa[2].logo} style={[styles.ImgWrp, { width: '33.33%' }]} />
                                    </View>
                                </View>
                                <View style={{ marginTop: 40, marginBottom: 20 }}>
                                    <View>
                                        <Text style={styles.partnertitlesty}>WHOLESALE</Text>
                                        <View style={styles.borderSty}><Text></Text></View>
                                    </View>
                                    <View style={styles.imgwarpsty}>
                                        <Image source={partner[0].wholesale[0].logo} style={[styles.ImgWrp, { width: '50%' }]} />
                                        <Image source={partner[0].wholesale[1].logo} style={[styles.ImgWrp, { width: '50%' }]} />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>



                </ScrollView>
            </View>
        );
    }
}



export default Reference;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30, textAlign: 'center' },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    overlay: {
        flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black',
        width: '100%'
    },
    renderImageWarp: {
        backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center',
        borderWidth: .5, borderColor: '#fff', width: '100%', padding: 5
    },
    ImgWrp: { resizeMode: 'contain', width: '100%', height: 80, },
    borderSty: {
        borderBottomWidth: 1, borderBottomColor: '#ccc', width: '20%', marginBottom: 10,
        marginLeft: 'auto', marginRight: 'auto'
    },
    partnertitlesty: { textAlign: 'center', color: '#de2d30', fontSize: 30, fontWeight: 'bold' },
    imgwarpsty: { flexDirection: 'row', width: '100%', resizeMode: 'contain', padding: 10 },




})