import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, Textarea, Form } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconANT from "react-native-vector-icons/AntDesign";


class Privacy extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>

                    <View style={styles.topimgwarp}>
                        <Image source={require('../../../../assets/images/img/privacy.jpg')} style={styles.topimg} />
                        <View style={[styles.overlay, { height: 200 }]}></View>
                        <View style={styles.toptextwarp}>
                            <Text style={styles.toptext}>{global.t('Privacy_Policy')}</Text>
                        </View>
                    </View>

                    <View style={{ padding: 20 }}>
                        <Text style={styles.subtitle}>Ice Holidays Sdn Bhd operates the </Text>
                        <TouchableOpacity>
                            <Text style={{ color: 'red', textAlign: 'center' }}>http://www.goldendestinations.com/ </Text>
                        </TouchableOpacity>
                        <Text style={styles.subtitle}>website, which provides the SERVICE.
                           This page is used to inform website visitors regarding our policies with the collection, use, and disclosure
                           of Personal Information if anyone decided to use our Service. If you choose to use our Service,
                           then you agree to the collection and use of information in relation with this policy. The Personal
                           Information that we collect are used for providing and improving the Service. We will not use or share your
                           information with anyone except as described in this Privacy Policy. The terms used in this Privacy Policy
                           have the same meanings as in our Terms and Conditions, which is accessible at
                        </Text>
                        <TouchableOpacity>
                            <Text style={{ color: 'red', textAlign: 'center' }}>http://www.goldendestinations.com/</Text>
                        </TouchableOpacity>
                        <Text style={styles.subtitle}>, unless otherwise defined in this Privacy Policy.</Text>

                    </View>

                    
                    <View style={{ paddingBottom: 100, padding: 20 }}>
                    
                    {/************ Information Collection and Use ************/}
                        <Text style={styles.contenttitle}>Information Collection and Use</Text>
                        <Text style={styles.contenttext}>
                            For a better experience while using our Service, we may require
                            you to provide us with certain personally identifiable information,
                            including but not limited to your name, phone number, and postal address.
                            The information that we collect will be used to contact or identify you.
                        </Text>

                        {/************ Log Data ************/}
                        <Text style={styles.contenttitle2}>Log Data</Text>
                        <Text style={styles.contenttext2}>
                            We want to inform you that whenever you visit our Service, we collect information
                            that your browser sends to us that is called Log Data. This Log Data may include
                            information such as your computer’s Internet Protocol (“IP”) address, browser
                            version, pages of our Service that you visit, the time and date of your visit,
                            the time spent on those pages, and other statistics.
                        </Text>

                        {/************ Cookies ************/}
                        <Text style={styles.contenttitle2}>Cookies</Text>
                        <Text style={styles.contenttext2}>
                            Cookies are files with small amount of data that is commonly used an anonymous unique
                            identifier. These are sent to your browser from the website that you visit and are
                            stored on your computer’s hard drive.
                        </Text>
                        <Text style={styles.contenttext3}>
                            Our website uses these “cookies” to collection information and to improve our Service.
                            You have the option to either accept or refuse these cookies, and know when a cookie
                            is being sent to your computer. If you choose to refuse our cookies, you may not be
                            able to use some portions of our Service.
                        </Text>

                        {/************ Service Providers ************/}
                        <Text style={styles.contenttitle2}>Service Providers</Text>
                        <Text style={styles.contenttext2}>
                            We may employ third-party companies and individuals due to the following reasons:
                        </Text>
                        <List style={{ color: '#777', marginTop: 5 }}>
                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="check" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>To facilitate our Service;</Text></View>
                            </ListItem>

                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="check" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>To provide the Service on our behalf;</Text></View>
                            </ListItem>

                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="check" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>To perform Service-related services; or</Text></View>
                            </ListItem>
                            <ListItem style={styles.ListItemstyle}>
                                <View style={styles.ListItemViewstyle} >
                                    <IconANT name="check" style={styles.ListItemiconstyle} />
                                </View>
                                <View><Text style={styles.icontext}>To assist us in analyzing how our Service is used.</Text></View>
                            </ListItem>
                        </List>
                        <Text style={styles.contenttext2}>
                            We want to inform our Service users that these third parties have access to your Personal Information.
                            The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not
                            to disclose or use the information for any other purpose.
                        </Text>

                        {/************ Security ************/}
                        <Text style={styles.contenttitle2}>Security</Text>
                        <Text style={styles.contenttext2}>
                            We value your trust in providing us your Personal Information, thus we are striving to use commercially
                            acceptable means of protecting it. But remember that no method of transmission over the internet,
                            or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.
                        </Text>

                        {/************ Links to Other Sites ************/}
                        <Text style={styles.contenttitle2}>Links to Other Sites</Text>
                        <Text style={styles.contenttext2}>
                            Our Service may contain links to other sites. If you click on a third-party link, you will be directed to
                            that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to
                            review the Privacy Policy of these websites, get more info. We have no control over, and assume no
                            responsibility for the content, privacy policies, or practices of any third-party sites or services.
                        </Text>

                        {/************ Children’s Privacy ************/}
                        <Text style={styles.contenttitle2}>Children’s Privacy</Text>
                        <Text style={styles.contenttext2}>
                            Our Services do not address anyone under the age of 13. We do not knowingly collect personal identifiable
                            information from children under 13. In the case we discover that a child under 13 has provided us with
                            personal information, we immediately delete this from our servers. If you are a parent or guardian and
                            you are aware that your child has provided us with personal information, please contact us so that we
                            will be able to do necessary actions.
                        </Text>

                        {/************ Changes to This Privacy Policy ************/}
                        <Text style={styles.contenttitle2}>Changes to This Privacy Policy</Text>
                        <Text style={styles.contenttext2}>
                            We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically
                            for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These
                            changes are effective immediately, after they are posted on this page.
                        </Text>

                        {/************ Contact Us ************/}
                        <Text style={styles.contenttitle2}>Contact Us</Text>
                        <Text style={styles.contenttext2}>
                            If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



export default Privacy;

const styles = StyleSheet.create({
    topimgwarp: { height: 200, },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', color: '#777', },
    contenttitle: { textAlign: 'left', color: '#000', fontWeight: 'bold', },
    contenttitle2: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 20 },
    contenttext: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext2: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext3: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext4: { textAlign: 'left', color: '#777', marginTop: 10, flexDirection: 'row' },
    ListItemstyle: { borderBottomWidth: 0, marginLeft: 0, paddingTop: 5, paddingBottom: 5 },
    ListItemViewstyle: { paddingRight: 10 },
    ListItemiconstyle: { fontSize: 15 },
    icontext: { color: '#777' },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.6, backgroundColor: 'black', width: '100%' },



})