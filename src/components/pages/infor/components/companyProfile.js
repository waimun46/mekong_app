import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, Accordion } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { dataArray, dataArray_2, dataArray_3 } from './data';
import MiceSpecial from './components/mice';
import IncentiveGroup from './components/incentive';



class CompanyProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataArray: dataArray,
            langPrint: ''
        }
    }

    componentDidMount() {
        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----InformationPage')
            this.setState({
                langPrint: langStorageRes
            })
        })
    }

    render() {
        //console.log(this.state.langPrint)
        const { langPrint } = this.state
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>
                    <View>
                        <View style={styles.topimgwarp}>
                            <Image source={require('../../../../assets/images/img/company.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('About_Us')}</Text>
                            </View>
                        </View>

                        <View style={styles.boxwarp}>
                            <Text style={styles.subtitle}>{global.t('Welcome_M_D')}</Text>
                            <Text style={styles.contenttext}>
                                {global.t('about_us_infor_1')}
                            </Text>
                            <Text style={[styles.contenttext, { paddingTop: 0 }]}>
                                {global.t('about_us_infor_2')}
                            </Text>

                            <Text style={[styles.contenttext, { paddingTop: 0,}]}>
                                {global.t('about_us_infor_3')}
                            </Text>
                        </View>
                        
                    </View>

                    <Content padder style={{ paddingBottom: 30 }}>
                        {/************************************ Accordion dataArray  ***********************************/}
                        <Accordion
                            dataArray={dataArray}
                            animation={true}
                            expanded={true}
                            renderHeader={(item, expanded) => (
                                <View style={styles.renderHeaderWarp}>
                                    <Text style={{ fontWeight: "600" }}>{" "}
                                        {langPrint === 'zh' ? (item.title_cn) : (item.title_en)}
                                    </Text>
                                    {expanded
                                        ? <Icon style={{ fontSize: 18 }} name="remove" />
                                        : <Icon style={{ fontSize: 18 }} name="add" />}
                                </View>
                            )}
                            renderContent={(item) => (
                                <View style={styles.renderContentWarp}>
                                    <View style={{ marginBottom: 5 }}>
                                        <Text style={styles.contenTitleStyle}>
                                            {langPrint === 'zh' ? (item.contentTitle_1_cn) : (item.contentTitle_1_en)}
                                        </Text>
                                        <Text style={{ lineHeight: 20 }}>{langPrint === 'zh' ? (item.content_1_cn) : (item.content_1_en)}</Text>

                                    </View>
                                    <View>
                                        <Text style={[styles.contenTitleStyle, { marginTop: 10 }]}>
                                            {langPrint === 'zh' ? (item.contentTitle_2_cn) : (item.contentTitle_2_en)}
                                        </Text>
                                        <Text style={{ lineHeight: 20 }}>{langPrint === 'zh' ? (item.content_2_cn) : (item.content_2_en)}</Text>
                                    </View>
                                </View >
                            )}
                            style={{ borderWidth: 0 }}
                        />

                        {/************************************ Accordion dataArray_2  ***********************************/}
                        <Accordion
                            dataArray={dataArray_2}
                            animation={true}
                            expanded={true}
                            renderHeader={(item, expanded) => (
                                <View style={styles.renderHeaderWarp}>
                                    <Text style={{ fontWeight: "600" }}>{" "}
                                        {langPrint === 'zh' ? (item.title_cn) : (item.title_en)}
                                    </Text>
                                    {expanded
                                        ? <Icon style={{ fontSize: 18 }} name="remove" />
                                        : <Icon style={{ fontSize: 18 }} name="add" />}
                                </View>
                            )}
                            renderContent={(item) => (
                                <View style={styles.renderContentWarp}>
                                    {
                                        item.subTitle_cn && item.subTitle_en === "" ? (
                                            null
                                        ) : (
                                                <View >
                                                    <Text style={[styles.contenTitleStyle, { marginBottom: 10 }]}>
                                                        {langPrint === 'zh' ? (item.subTitle_cn) : (item.subTitle_en)}
                                                    </Text>
                                                </View>
                                            )
                                    }

                                    <View style={{ marginBottom: 5 }}>
                                        <Text style={styles.contenTitleStyle}>
                                            {langPrint === 'zh' ? (item.contentTitle_1_cn) : (item.contentTitle_1_en)}
                                        </Text>
                                        <Text style={{ lineHeight: 20 }}>{langPrint === 'zh' ? (item.content_1_cn) : (item.content_1_en)}</Text>
                                    </View>
                                    <View>
                                        <Text style={[styles.contenTitleStyle, { marginTop: 10 }]}>
                                            {langPrint === 'zh' ? (item.contentTitle_2_cn) : (item.contentTitle_2_en)}
                                        </Text>
                                        <Text style={{ marginBottom: 5, lineHeight: 20 }}>{langPrint === 'zh' ? (item.content_2_cn) : (item.content_2_en)}</Text>
                                        {
                                            item.content_2b_cn && item.content_2b_en === "" ? (
                                                null
                                            ) : (
                                                    <Text style={{ lineHeight: 20 }}>{langPrint === 'zh' ? (item.content_2b_cn) : (item.content_2b_en)}</Text>
                                                )
                                        }
                                    </View>
                                </View >
                            )}
                            style={{ borderWidth: 0 }}
                        />


                        {/************************************ Accordion dataArray_3  ***********************************/}
                        <Accordion
                            dataArray={dataArray_3}
                            animation={true}
                            expanded={true}
                            renderHeader={(item, expanded) => (
                                <View style={styles.renderHeaderWarp}>
                                    <Text style={{ fontWeight: "600" }}>{" "}
                                        {langPrint === 'zh' ? (item.title_cn) : (item.title_en)}
                                    </Text>
                                    {expanded
                                        ? <Icon style={{ fontSize: 18 }} name="remove" />
                                        : <Icon style={{ fontSize: 18 }} name="add" />}
                                </View>
                            )}
                            renderContent={(item) => (
                                <View style={styles.renderContentWarp}>
                                    {
                                        item.subTitle_cn && item.subTitle_en === "" ? (
                                            null
                                        ) : (
                                                <View >
                                                    <Text style={[styles.contenTitleStyle, { marginBottom: 10 }]}>
                                                        {langPrint === 'zh' ? (item.subTitle_cn) : (item.subTitle_en)}
                                                    </Text>
                                                </View>
                                            )
                                    }
                                    <View>
                                        <View style={{ marginBottom: 5 }}>
                                            <Text style={styles.contenTitleStyle}>
                                                {langPrint === 'zh' ? (item.contentTitle_1_cn) : (item.contentTitle_1_en)}
                                            </Text>
                                            <Text style={{ lineHeight: 20 }}>
                                                {langPrint === 'zh' ? (item.content_1_cn) : (item.content_1_en)}
                                            </Text>
                                        </View>
                                        <View>
                                            <Text style={[styles.contenTitleStyle, { marginTop: 10 }]}>
                                                {langPrint === 'zh' ? (item.contentTitle_2_cn) : (item.contentTitle_2_en)}
                                            </Text>
                                            <Text style={{ marginBottom: 5, lineHeight: 20 }}>
                                                {langPrint === 'zh' ? (item.content_2_cn) : (item.content_2_en)}
                                            </Text>
                                        </View>
                                        <View>
                                            <Text style={[styles.contenTitleStyle, { marginTop: 10 }]}>
                                                {langPrint === 'zh' ? (item.contentTitle_3_cn) : (item.contentTitle_3_en)}
                                            </Text>
                                            <Text style={{ marginBottom: 5, lineHeight: 20 }}>
                                                {langPrint === 'zh' ? (item.content_3_cn) : (item.content_3_en)}
                                            </Text>
                                        </View>
                                        <View>
                                            <Text style={[styles.contenTitleStyle, { marginTop: 10 }]}>
                                                {langPrint === 'zh' ? (item.contentTitle_4_cn) : (item.contentTitle_4_en)}
                                            </Text>
                                            <Text style={{ marginBottom: 5, lineHeight: 20 }}>
                                                {langPrint === 'zh' ? (item.content_4_cn) : (item.content_4_en)}
                                            </Text>
                                        </View>

                                    </View>

                                </View >
                            )}
                            style={{ borderWidth: 0 }}
                        />
                    </Content>

                    {/************************************ MiceSpecial ***********************************/}
                    <MiceSpecial />

                    {/************************************ IncentiveGroup ***********************************/}
                    <IncentiveGroup />


                </ScrollView>
            </View>
        );
    }
}



export default CompanyProfile;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', marginTop: 20, padding: 20, fontSize: 23, color: '#de2d30', fontWeight: 'bold' },
    contenttitle: { textAlign: 'center', padding: 10, color: '#000', fontWeight: 'bold', fontSize: 20, },
    contenttext: { textAlign: 'center', padding: 25, paddingBottom: 10, color: '#777', lineHeight: 20, paddingTop: 5 },
    contenttext2: { textAlign: 'center', padding: 20, marginTop: -10, color: '#777', },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black', width: '100%' },
    boxwarp: { paddingBottom: 10, },
    contenTitleStyle: { fontWeight: 'bold', marginBottom: 5, color: '#de2d30', },
    renderHeaderWarp: {
        flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center",
        backgroundColor: "#ececec", marginBottom: 5,
    },
    renderContentWarp: { backgroundColor: "#fcf5f5", padding: 10, marginBottom: 10, marginTop: -3, },



})