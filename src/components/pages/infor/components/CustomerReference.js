import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, } from 'react-native';
import { Tab, Tabs, } from 'native-base';
import Mekong from './components/mekong';
import IPT from './components/ipt';

class CustomerReference extends Component {

    render() {

        return (
            <View style={{ flex: 1, }}>
                <ScrollView>

                    <Tabs tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}>
                        <Tab
                            heading={global.t('ikhmertrip')}
                            tabStyle={{ backgroundColor: "white" }}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                            style={{ backgroundColor: 'transperant' }}
                        >
                            <Mekong />
                        </Tab>
                        <Tab
                            heading={global.t('IPT_Cop')}
                            tabStyle={{ backgroundColor: "white" }}
                            textStyle={{ color: '#ccc', }}
                            activeTextStyle={{ color: '#de2d30' }}
                            activeTabStyle={{ backgroundColor: "white", }}
                            tabContainerStyle={{ backgroundColor: "#de2d30" }}
                            style={{ backgroundColor: 'transperant' }}
                        >
                            <IPT />
                        </Tab>
                    </Tabs>

                </ScrollView>
            </View>

        );
    }
}



export default CustomerReference;

const styles = StyleSheet.create({





})