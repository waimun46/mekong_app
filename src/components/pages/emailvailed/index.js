import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground, Image, Dimensions } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';


class EmailVailed extends Component {

    render() {
        return (
            <View style={{ backgroundColor: 'white', }}>
                <ScrollView >
                    <View >
                        <ImageBackground
                            source={require('../../../assets/images/img/email.png')}
                            style={styles.ImageBackgroundStyle}
                        >
                        </ImageBackground>
                        <View style={styles.BottomContainer}>
                            <View>
                                <Text style={styles.TextTitle}>Congratulations!</Text>
                                <Text style={styles.TextStyle}>
                                    You're all signed up! Please check your email for activate your mail
                            </Text>
                            </View>
                            <View style={styles.BottomTextContainer}>
                                <Text style={{ width: '50%', color:'#90949c' }}>60s</Text>
                                <Text style={styles.BottomText}>Back to Home</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default EmailVailed;

const styles = StyleSheet.create({
    ImageBackgroundStyle: {
        width: '100%', height: 300, resizeMode: 'cover', flex: 1, alignItems: 'center',
        justifyContent: 'center',
        flex: 1, height: Dimensions.get('window').width
    },
    TextTitle: { textAlign: 'center', fontSize: 30, color: '#de2d30', fontWeight: 'bold', marginTop: 50 },
    TextStyle: { textAlign: 'center', fontSize: 20, marginTop: 20, color: '#00953b' },
    BottomContainer: { padding: 40, height: 400, flex: 1, alignItems: 'center', },
    BottomTextContainer: {
        width: '100%', flex: 1, flexDirection: 'row', height: 50, justifyContent: 'center',
        alignItems: 'center', position: 'absolute', marginBottom: 30, bottom: 50
    },
    BottomText: { alignItems: 'flex-end', width: '50%', textAlign: 'right',fontWeight: 'bold', }
})
