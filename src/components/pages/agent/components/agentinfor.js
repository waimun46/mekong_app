import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, Platform, WebView } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import { NavigationApps, actions, googleMapsTravelModes, mapsTravelModes } from "react-native-navigation-apps";
import call from 'react-native-phone-call'


class AgentInfor extends Component {

    constructor(props) {
        super(props);
        
    }
    
    call = () => {
        const args = {
            number: this.props.data.tel,
            prompt: false,
        };
        call(args).catch(console.error);
    };

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView>
                    <View style={{ backgroundColor: 'white', paddingBottom: Platform.OS === 'ios' ? 200 : 100 }}>
                        <Image source={require('../../../../assets/images/img/map2.png')} style={styles.Imagestyletop} />
                        <View>
                            <View style={styles.imageProfileWarp}>
                                <Image source={{ uri: this.props.data.agentlogo }} style={styles.imageProfileImg} />
                            </View>
                            <TouchableOpacity onPress={this.call} style={styles.IconWarp} >
                                <View >
                                    <IconMCI name="phone-in-talk" style={styles.IconStyle} />
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View>
                            <View style={{ padding: 20 }}>
                                <Text style={styles.TitleStyle}> {this.props.data.company_name}</Text>
                                <Text style={styles.TextStyle2} >{this.props.data.state}</Text>
                                <Text style={styles.TextStyle} >
                                    {this.props.data.add1} {this.props.data.add2}
                                </Text>
                                <Text style={styles.TextStyle} >
                                    {this.props.data.email}
                                </Text>
                                <Text style={styles.TextStyle2}  >Tel: {this.props.data.tel}</Text>
                                <Text style={styles.TextStyle2}  >Mobile: {this.props.data.mobile}</Text>
                                <Text style={styles.TextStyle2} >Latitude {this.props.data.latitude}, {this.props.data.longitude}</Text>
                            </View>
                        </View>

                        <View style={{ textAlign: 'center', marginLeft: 'auto', marginRight: 'auto', marginTop: Platform.OS === 'ios' ? 50 : 30 }}>
                            <NavigationApps
                                iconSize={70}
                                row
                                address={this.props.data.add2}
                                waze={{ action: actions.navigateByAddress }}
                                googleMaps={{ lat: this.props.data.latitude, lon: this.props.data.longitude, action: actions.navigateByAddress }}
                                maps={{ action: actions.navigateByAddress }}
                            />
                        </View>
                    </View>

                </ScrollView>
            </View>
        );
    }
}


export default AgentInfor;

const styles = StyleSheet.create({
    Imagestyletop: { resizeMode: 'cover', width: '100%', height: 130, },
    imageProfileWarp: { marginTop: -90, marginLeft: 'auto', marginRight: 'auto', backgroundColor: 'white', padding: 3 },
    imageProfileImg: { resizeMode: 'cover', width: 150, height: 150, },
    IconWarp: { position: 'absolute', right: 30, marginTop: -30, backgroundColor: '#14943f', padding: 10, borderRadius: 100 },
    IconStyle: { fontSize: 30, color: 'white', alignItems: 'center', },
    TitleStyle: { textTransform: 'uppercase', textAlign: 'center', fontSize: 25, color: '#de2d30' },
    TextStyle: { textAlign: 'center', marginTop: 10, marginBottom: 10, fontSize: 15, color: '#535a60' },
    TextStyle2: { textAlign: 'center', marginTop: 5, marginBottom: 5, fontSize: 15, color: '#848d95' },

})
