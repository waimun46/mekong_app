import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { DestinationApi } from '../../../../../../PostApi';

const numColumns = 3;

class CountryData extends Component {

    constructor(props) {
        super(props);

    }

    /************************* Render Item in FlatList *************************/
    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => Actions.countryinfor()} style={{ width: '33.33%', }}>
                <View>
                    <ImageBackground source={{ uri: item.picture_url }} style={styles.item}>
                        <Text style={styles.TextImage}>{item.title}</Text>
                    </ImageBackground>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        console.log(this.props.data);
        return (
            <FlatList
                data={this.props.data}
                renderItem={this.renderItem}
                numColumns={numColumns}
                style={styles.Flatcontainer}
            />
        );
    }
}


export default CountryData;

const styles = StyleSheet.create({
    Flatcontainer: {
        flex: 1,
        marginVertical: 10,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 2,
        height: Dimensions.get('window').width / numColumns,
    },
    TextImage: {
        fontSize: 20, color: 'white', fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000',
    }
})
