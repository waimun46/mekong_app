import React, { Component } from 'react';
import { Platform, FlatList, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { TourGetDetialApi } from '../../../../../../PostApi';
import { Actions } from 'react-native-router-flux';
import IconION from "react-native-vector-icons/Ionicons";
import HTML from 'react-native-render-html';
import IconANT from "react-native-vector-icons/AntDesign";
import { GetInforDetialApi } from '../../../../../../PostApi';

const numColumns = 2;


const dataImg = [
    {
        "imgrp": "https://www.iol.co.za/assets/images/general/no-image.png"
    }
]

class CountryInfor extends Component {

    constructor(props) {
        //console.log(props,'props')
        super(props);
        this.state = {
            //datamap: [this.props.data.destination_list]
            //dataTour: [] 

            dataTour: []
        }
    }

    renderItem = ({ item }) => {
        //console.log(item);
        return (

            <View style={styles.ImageWarpper}>
                <TouchableOpacity onPress={() => Actions.countryinforlist({
                    dataTour: item,
                    type_id: this.props.data.type_id,
                    country_id: this.props.data.id
                })}>
                    <View style={styles.ImageInner} >
                        <View style={styles.ImageInnerWarp}>
                            <Image source={{ uri: item.picture_url === "" ? dataImg[0].imgrp : item.picture_url }}
                                style={styles.ImageStyle} />
                        </View>
                        <View style={{ borderRadius: 5, }}>
                            <Text style={styles.TextImage}>{item.destination}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    ListEmpty = () => {
        return (
            <View style={styles.MainContainer}>
                <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
            </View>
        );
    };

    render() {
        //console.log(this.props.data);

        /************************* Pass props data in htmlContent using(react-native-render-html) *************************/
        const htmlContent = this.props.data.description;

        /************************* Pass props data using map functions *************************/
        // const datapost = this.props.data.destination_list;
        // const datalist = datapost.map((item, i) => {

        //     //console.log(item);
        //     console.log(this.props.data.type_id)
        //     return (
        //         <View style={styles.ImageWarpper} key={i}>
        //             <TouchableOpacity onPress={() => Actions.countryinforlist({
        //                 dataTour: item,
        //                 type_id: this.props.data.type_id,
        //                 country_id: this.props.data.id
        //             })}>
        //                 <View style={styles.ImageInner} >
        //                     <View style={styles.ImageInnerWarp}>
        //                         <Image source={{ uri: item.picture_url === "" ? dataImg[0].imgrp : item.picture_url }}
        //                             style={styles.ImageStyle} />
        //                     </View>
        //                     <View style={{ borderRadius: 5, }}>
        //                         <Text style={styles.TextImage}>{item.destination}</Text>
        //                     </View>
        //                 </View>
        //             </TouchableOpacity>
        //         </View>
        //     )
        // })

        return (
            <View>
                <ScrollView>
                    <View style={{ backgroundColor: '#fff' }}>
                        <View>
                            <Image
                                source={{ uri: this.props.data.pic_url === "" ? dataImg[0].imgrp : this.props.data.pic_url }}
                                style={styles.CountryInforImage}
                            />
                        </View>
                        <View style={{ padding: 20 }} >
                            <IconION name="ios-briefcase" size={35} style={styles.IconStyle} />
                            <Text style={styles.TitleText}>{this.props.data.title}</Text>
                            {/* <Text style={styles.ContentText}>{this.props.data.description}</Text> */}
                            <HTML
                                html={htmlContent}
                                style={styles.ContentText}
                            />
                        </View>
                    </View>
                    <View style={{ padding: 15, }}>

                        <FlatList
                            data={this.props.data.destination_list}
                            renderItem={this.renderItem}
                            numColumns={numColumns}
                            style={styles.FlatContainer}
                            ListEmptyComponent={this.ListEmpty}
                        />

                        {/* <View style={styles.ImageWarpperContainer} >
                            {datalist}
                        </View> */}
                    </View>
                </ScrollView>
            </View>

        );
    }
}



export default CountryInfor;

const styles = StyleSheet.create({
    CountryInforImage: { width: "100%", resizeMode: 'cover', height: 180, },
    IconStyle: {
        textAlign: 'center',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    TitleText: {
        textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
    FlatContainer: { flex: 1, marginVertical: 10, },
    ImageWarpperContainer: { flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' },
    ImageWarpper: { width: '50%', paddingLeft: 10, paddingRight: 10, marginBottom: 20, },
    ImageInner: { backgroundColor: 'white', borderRadius: 10, },
    ImageInnerWarp: { borderTopRightRadius: 5, borderTopLeftRadius: 5, overflow: 'hidden', },
    ImageStyle: { height: 150, width: null, flex: 1, },
    TextImage: {
        textAlign: 'center', padding: 10, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    MainContainer: { justifyContent: 'center', flex: 1, marginTop: 100, },

})