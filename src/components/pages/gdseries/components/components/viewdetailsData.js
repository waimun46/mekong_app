import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator, AsyncStorage } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { TourGetDetialApi } from '../../../../../../PostApi'
import IconFOU from "react-native-vector-icons/Foundation";
import IconANT from "react-native-vector-icons/AntDesign";

// keyExtractor = (item) => item.key;

const localdata = [
    { "airlinelocal": "To Be Confirmed", "statuslocal": "Selling Fast" }
]

class ViewDetailData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 0,
            country: this.props.country_id,
            keyword: this.props.keyword_Id,
            titleid: this.props.dataInfor.title_id,
            isLoading: true,
            dataList: []
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('TYPE_ID').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------TYPE_ID')
            this.setState({
                type: asyncStorageRes
            })
            //console.log(this.state.tokenId, '-------------fetchDataApi1111')

            this.fetchApiTour();
        });

    }

    fetchApiTour(type = this.state.type, country = this.state.country, keyword = this.state.keyword) {
        //console.log(keyword, country, type, 'keywordId-----ViewDetailData');
        let Url = `https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&type=${type}&country=${country}&keyword=${keyword}`;
        fetch(Url)
            .then(res => res.json())
            .then(json => {
                console.log(json, 'fetchApiTour');
                this.setState({
                    dataList: json,
                    isLoading: false,

                });
            });
    }


    renderItem = ({ item }) => {
        // console.log(localdata, 'item--------------ViewDetailData')
        // console.log(item, 'item--------------dataList')
        return (
            <List style={{ backgroundColor: 'white', marginTop: 10, marginBottom: 5, marginLeft: 0 }}>
                <ListItem thumbnail style={{ marginLeft: 0, }}>
                    <Body style={styles.bodystyle}>
                        <View style={{ backgroundColor: '#fcf5f5' }}>
                            <Text note style={styles.texttitlestyle}>{item.title}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ padding: 15, width: '75%' }}>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Tour_Code')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.tour_code}</Text>
                                </View>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Airline')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal : item.airline}</Text>
                                </View>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Status')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.status === "" ? localdata[0].statuslocal : item.status}</Text>
                                </View>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Departure')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.departure}</Text>
                                </View>

                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text note style={{ width: '30%', color: '#00953b' }}>{global.t('Price')}: </Text>
                                    <Text note style={styles.Textprice}>{item.price}</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: '#979999', padding: 5, width: '25%', }}>
                                <TouchableOpacity style={styles.dwnwarp} onPress={this.downloadFile.bind(this, item)}>
                                    <IconFOU name="download" size={40} style={styles.IconColor} />
                                    <Text note style={styles.dwntext}>{global.t('Itinerary')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Body>
                </ListItem>
            </List>
        )
    }

    /************************* Download PDF File *************************/
    downloadFile() {
        function getLocalPath(url) {
            const filename = url.split('/').pop();
            return `${RNFS.DocumentDirectoryPath}/${filename}`;
        }

        const url = this.props.dataInfor.itinerary;
        const localFile = getLocalPath(url);

        const options = {
            fromUrl: url,
            toFile: localFile
        };
        RNFS.downloadFile(options).promise
            .then(() => FileViewer.open(localFile))
            .then(() => {
                // success
            })
            .catch(error => {
                // error
            });
    }


    ListEmpty = () => {
        return (
            <View style={styles.MainContainer}>
                <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
            </View>
        );
    };


    render() {

        const titleprops = this.props.dataInfor.title;
        //console.log(titleprops, 'titleprops');

        const { isLoading } = this.state;

        /************************* find match props title data list *************************/
        const unique = this.state.dataList;
        const res = unique.filter(i => titleprops.includes(i.title));

        return (
            <Content style={{ borderBottomWidth: 0 }}>

                {
                    isLoading ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                            <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                            <ActivityIndicator size="large" color="#de2d30" />
                        </View>
                    ) : (
                            <FlatList
                                data={res}
                                renderItem={this.renderItem}
                                keyExtractor={this.keyExtractor}
                                ListEmptyComponent={this.ListEmpty}
                            />
                        )
                }



                {/* <List style={{ backgroundColor: 'white',borderBottomWidth: 0 }}>
                    <ListItem thumbnail style={{ marginLeft: 0, borderBottomWidth: 0 }}>

                        <Body style={{ width: '75%',borderBottomWidth: 0  }}>
                            <Text style={styles.TextTitle}>{this.props.dataInfor.tour_code}</Text>
                            <View style={styles.textWarp}>
                                <Text note style={{ width: '28%' }}>{global.t('Title')}: </Text>
                                <Text note style={styles.TextInner}>{this.props.dataInfor.title}</Text>
                            </View>
                             <View style={styles.textWarp}>
                                <Text note style={{ width: '28%' }}>{global.t('Destination')}: </Text>
                                <Text note style={styles.textLoc}>{this.props.data.destination}</Text>
                            </View>
                            <View style={styles.textWarp}>
                                <Text note style={{ width: '28%' }}>{global.t('Type')}: </Text>
                                <Text note style={styles.textLoc}>{this.props.data.type}</Text>
                            </View>
                            <View style={styles.textWarp}>
                                <Text note style={{ width: '28%' }}>{global.t('Airline')}: </Text>
                                <Text note style={styles.TextInner}>{this.props.data.airline}</Text>
                            </View> 
                            <View style={styles.textWarp}>
                                <Text note style={{ width: '28%' }}>{global.t('Departure')}: </Text>
                                <Text note style={styles.TextInner}>{this.props.dataInfor.departure}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text note style={{ width: '28%', color: '#00953b' }}>{global.t('Price')}: </Text>
                                <Text note style={styles.Textprice}>RM {this.props.dataInfor.price}</Text>
                                <TouchableOpacity style={styles.dwnwarp} onPress={() => this.downloadFile()}>
                                    <Text note style={styles.downbtn}>{global.t('Download')}</Text>
                                </TouchableOpacity>
                            </View>
                        </Body>

                        <Right style={{ width: '25%', paddingTop: 0, paddingRight: 0, paddingBottom: 0 }}>
                            <View style={{ width: '100%' }}>
                                <View style={{ backgroundColor: '#f3f3f3', }}>
                                    <Text style={{ padding: 10, color: '#de2d30', textAlign: 'center' }}>{global.t('Status')}</Text>
                                </View>
                                <View style={{ backgroundColor: '#faa058', paddingTop: 35, paddingBottom: 35 }}>
                                    <Text style={{ padding: 10, paddingRight: 5, paddingLeft: 5, color: '#fff', textAlign: 'center' }}>{this.props.dataInfor.status}</Text>
                                </View>
                            </View>

                        </Right>
                    </ListItem>
                </List> */}
            </Content>

        );
    }
}


export default ViewDetailData;

const styles = StyleSheet.create({
    IconColor: { color: '#fff', textAlign: 'center', paddingTop: 30, },
    TextTitle: { color: '#de2d30', fontWeight: 'bold', },
    TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
    TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, },
    Textprice: { color: '#00953b', width: '35%' },
    textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
    textWarp: { flexDirection: 'row', marginTop: 2, },
    downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
    dwnwarp: { width: '100%', },
    bodystyle: {
        width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
        paddingTop: 0, borderBottomWidth: 0
    },
    texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
    dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#fff' },
    MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },

})
