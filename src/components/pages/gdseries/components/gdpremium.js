import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Dimensions, TouchableOpacity, ImageBackground } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import CountryData from './components/countrydata';
import { DestinationApi } from '../../../../../PostApi';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'
const numColumns = 3;

const dataImg = [
    {
        "imgrp": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e"
    }
]

class GDPremium extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() { 

    }


    /************************* Render Item in FlatList *************************/
    renderItem = ({ item }) => {
        console.log(dataImg, 'GDPremium------------imgrp-------item');
        return (
            <TouchableOpacity onPress={() => Actions.countryinfor({ data: item })} style={{ width: '33.33%', }}>
                <View>

                    <ImageBackground source={{ uri: item.pic_url === "" ? dataImg[0].imgrp : item.pic_url }} style={styles.item}>
                        <Text style={styles.TextImage}>{item.title}</Text>
                    </ImageBackground>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const htmlContent = this.props.data[0].type_description;
        console.log(this.props.data[0], '-------props')
        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                    <View >
                        <Image source={{ uri: this.props.data[0].type_pic }} style={styles.PremiumImage} />
                    </View>
                    <View>

                        <Tabs style={{ backgroundColor: 'white' }} locked tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}>
                            <Tab
                                heading={global.t('Information')}
                                tabStyle={{ backgroundColor: "white" }}
                                textStyle={{ color: '#ccc', }}
                                activeTextStyle={{ color: '#de2d30' }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}

                            >

                                <HTML html={htmlContent}
                                    imagesInitialDimensions={{ width: '60%', height: 200 }}
                                    tagsStyles={{
                                        p: { width: '100%' },
                                        h2: { padding: 20, fontSize: 16, marginBottom: -30 },
                                        em: { fontSize: 30, },
                                    }}
                                    ignoredStyles={['font-family', 'display',]}
                                    ignoredTags={[...IGNORED_TAGS, 'iframe', 'img',]}
                                />

                        
                            </Tab>

                            <Tab heading={global.t('Country')}
                                tabStyle={{ backgroundColor: "white" }}
                                textStyle={{ color: '#ccc', }}
                                activeTextStyle={{ color: '#de2d30' }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                            >
                                <FlatList
                                    data={this.props.data[0]}
                                    renderItem={this.renderItem}
                                    numColumns={numColumns}
                                    style={styles.Flatcontainer}
                                />
                            </Tab>

                        </Tabs>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default GDPremium;

const styles = StyleSheet.create({
    PremiumImage: { width: "100%", resizeMode: 'cover', height: 180, },
    Flatcontainer: {
        flex: 1,
        marginVertical: 10,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 2,
        height: Dimensions.get('window').width / numColumns,
    },
    TextImage: {
        fontSize: 18, color: 'white', fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000', textAlign: 'center'
    }
})
