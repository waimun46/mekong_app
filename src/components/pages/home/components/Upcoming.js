import React, { Component } from 'react';
import { Platform, StyleSheet, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import IconANT from "react-native-vector-icons/AntDesign";
import { Actions } from 'react-native-router-flux';
import { MonthGetDetialApi } from '../../../../../PostApi';

/************************* If Api Data Empty Show Local data *************************/
const localdata = [
  { "airlinelocal": "To Be Confirmed", "statuslocal": "Selling Fast" }
]

class Upcoming extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      dataU: [],
    }
  }

  componentDidMount() {

    /************************* Fetch Data in PostApi *************************/
    MonthGetDetialApi().then((fetchData) => {
      //console.log(fetchData, 'fetch')
      this.setState({
        data: fetchData,
        isLoading: false
      })
    })

  }

  /************************* Key Flatlist *************************/
  keyExtractor = (item, index) => item.key;

  /************************* RenderItem in Flatlist *************************/
  renderItem = ({ item, index }) => {
    //console.log(localdata, 'localdata----------item')
    return (
      <ListItem thumbnail style={{ marginBottom: 10, marginTop: 10, marginLeft: 5, flex: 1 }}>
        <Left style={styles.leftwarp}>
          <Thumbnail style={{ position: 'relative', height: 70, width: 70 }}
            square source={require('../../../../assets/images/background/blackbg.jpg')}
          />
          <View style={styles.ThumbnailTextContainer}>
            <Text style={styles.ThumbnailText1}>{item.departure}</Text>
          </View>
        </Left>
        <Body style={styles.bodyStyle}>
          <View style={{ marginLeft: 40 }}>
            <Text style={styles.TitleList}>{item.title}</Text>
            <Text note>{global.t('Tour_Code')}:
            <Text style={styles.titlecolor}> {item.tour_code}</Text>
            </Text>
            <Text note >{global.t('Airline')}:
            <Text style={styles.titlecolor}> {item.airline === null ? localdata[0].airlinelocal : item.airline}</Text>
            </Text>
            <Text note >{global.t('Status')}:
            <Text style={styles.titlecolor}> {item.status === "" ? localdata[0].statuslocal : item.status}</Text>
            </Text>

            <View style={{ flexDirection: 'row', width: '100%' }}>
              <View style={{ width: '50%' }}>
                <Text note >{global.t('Price')}:
                <Text style={styles.price}> RM {item.price}</Text>
                </Text>
              </View>
              <TouchableOpacity onPress={() => Actions.upcomingdetail({ data: item })} style={{ width: '50%' }}>
                <Text style={styles.viewmore} uppercase={false}>{global.t('More_Details')} ></Text>
              </TouchableOpacity>
            </View>
          </View>
        </Body>
      </ListItem>
    )
  }

  render() {
    //console.log(this.state.datalocal, 'render----------datalocal')
    const unique = this.state.dataU;
    this.state.data.map(x => unique.filter(a => a.title == x.title).length > 0 ? null : unique.push(x));
    //console.log(unique, 'unique');

    const { isLoading } = this.state;
    return (
      <View style={{ marginTop: 20, }}>
        <Text style={styles.TitleText}>{global.t('Upcoming_Departure')}</Text>
        <Content style={{ paddingBottom: 100 }}>
          <List>
            {
              isLoading ? (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                  <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                  <ActivityIndicator size="large" color="#de2d30" />
                </View>
              ) : (
                  <FlatList
                    data={unique}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                  />
                )
            }
          </List>
          <TouchableOpacity onPress={() => Actions.gdSeries()}>
            <Text style={styles.TextMore}> {global.t('More')} <IconANT name='right' size={15} /></Text>
          </TouchableOpacity>
        </Content>
      </View>
    );
  }
}

export default Upcoming;

const styles = StyleSheet.create({
  TitleText: { textAlign: 'left', fontSize: 18, padding: 10, fontWeight: 'bold', },
  ThumbnailTextContainer: { position: 'absolute', width: '100%', textAlign: 'center', },
  ThumbnailText1: {
    color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', padding: 7,
    ...Platform.select({
      ios: { paddingTop: 15, },
      android: { paddingTop: 15, }
    }),

  },
  ThumbnailText2: {
    color: 'white', fontWeight: 'bold',
    ...Platform.select({
      android: { marginTop: -4 }
    }),
  },
  TitleList: { fontWeight: 'bold', marginBottom: 5, color: '#606c77' },
  TextMore: { paddingBottom: 20, textAlign: 'right', padding: 20, color: '#de2d30' },
  TouchableOpacity_Inside_Text: { padding: 30, textAlign: 'center', color: '#de2d30' },
  viewmore: { color: '#de2d30', paddingRight: 10, paddingLeft: 8, textAlign: 'right', fontSize: 14, },
  titlecolor: { color: '#606c77', fontSize: 14 },
  price: { color: 'green', fontSize: 14, width: '50%' },
  leftwarp: {
    position: 'absolute', top: 0, zIndex: 1,
    ...Platform.select({
      ios: { elevation: 0 },
      android: { elevation: 10 }
    })
  },
  bodyStyle: {
    backgroundColor: 'white', padding: 10, marginRight: 10, borderWidth: 1, borderRadius: 2,
    borderColor: '#ddd', borderBottomWidth: 0, shadowColor: '#000', shadowOffset: { width: 3, height: 2 },
    shadowOpacity: 0.3, shadowRadius: 2, elevation: 8, marginLeft: 30, marginRight: 10, marginTop: 10, zIndex: -1
  }

})

