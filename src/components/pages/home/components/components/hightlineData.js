import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconFOU from "react-native-vector-icons/Foundation";
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';




/************************* If Api Data Empty Show Local data *************************/
const localdata = [
    { "airlinelocal": "To Be Confirmed", "statuslocal": "Selling Fast", "pricelocal": "To Be Confirmed" }
]


class HightLineData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }

    }

    componentDidMount() {
        this.getData();
    }

    getData(keyword = this.props.keyword_id, tourcode = this.props.tour_id) {
        let url = `https://goldendestinations.com/api/new/tour_highlights_detail.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&keyword=${keyword}&tourcode=${tourcode}`;
        const obj = this;
        fetch(url).then((res) => res.json())
            .then(function (myJson) {
                //console.log(myJson, '------myJson')
                obj.setState({
                    data: myJson,
                    isLoading: false
                })
            })
    }

    keyExtractor = (item) => item.key;

    /************************* renderItem *************************/
    renderItem = ({ item }) => {
        //console.log(item, '--------item------props')

        return (
            <List style={{ backgroundColor: 'white', marginTop: 10, marginBottom: 5, marginLeft: 0 }}>
                <ListItem thumbnail style={{ marginLeft: 0, }}>
                    <Body style={styles.bodystyle}>
                        <View style={{ backgroundColor: '#fcf5f5', }}>
                            <Text note style={styles.texttitlestyle}>{item.title}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ padding: 15, width: '75%' }}>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Tour_Code')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.tour_code}</Text>
                                </View>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Status')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.status === "" ? localdata[0].statuslocal : item.status}</Text>
                                </View>
                                <View style={styles.textWarp}>
                                    <Text note style={{ width: '30%' }}>{global.t('Departure')}: </Text>
                                    <Text note style={styles.TextInner2}>{item.departure}</Text>
                                </View>

                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text note style={{ width: '30%', color: '#00953b' }}>{global.t('Price')}: </Text>
                                    <Text note style={styles.Textprice}>{item.price === "" ? localdata[0].pricelocal : item.price}</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: '#979999', padding: 5, width: '25%', }}>
                                <TouchableOpacity style={styles.dwnwarp} onPress={this.downloadFile.bind(this, item)}>
                                    <IconFOU name="download" size={40} style={styles.IconColor} />
                                    <Text note style={styles.dwntext}>{global.t('Itinerary')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Body>

                </ListItem>
            </List>
        )
    }

    /************************* ListEmpty show content *************************/
    ListEmpty = () => {
        return (
            <View style={styles.MainContainer}>
                <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
            </View>
        );
    };

    /************************* Download PDF File *************************/
    downloadFile(item) {
        function getLocalPath(url) {
            const filename = url.split('/').pop();
            return `${RNFS.DocumentDirectoryPath}/${filename}`;
        }

        const url = item.itinerary;
        const localFile = getLocalPath(url);

        const options = {
            fromUrl: url,
            toFile: localFile
        };
        RNFS.downloadFile(options).promise
            .then(() => FileViewer.open(localFile))
            .then(() => {
                // success
            })
            .catch(error => {
                // error
            });
    }



    render() {
        const { isLoading, data } = this.state;
        console.log(this.props.tour_id, '--------tour_id');
        console.log(this.props.keyword_id, '---------keyword_id')


        return (
            <Content style={{ borderBottomWidth: 0 }}>
                {
                    isLoading ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                            <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                            <ActivityIndicator size="large" color="#de2d30" />
                        </View>
                    ) : (
                            <FlatList
                                data={data}
                                renderItem={this.renderItem}
                                keyExtractor={this.keyExtractor}
                                ListEmptyComponent={this.ListEmpty}
                            />
                        )
                }

            </Content>

        );
    }
}


export default HightLineData;

const styles = StyleSheet.create({

    IconColor: { color: '#fff', textAlign: 'center', paddingTop: 20, },
    TextTitle: { color: '#de2d30', fontWeight: 'bold', },
    TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
    TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, },
    Textprice: { color: '#00953b', width: '35%' },
    textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
    textWarp: { flexDirection: 'row', marginTop: 2, },
    downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
    dwnwarp: { width: '100%', },
    bodystyle: {
        width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
        paddingTop: 0, borderBottomWidth: 0
    },
    texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
    dwntext: { marginTop: 10, textAlign: 'center', marginRight: 0, color: '#fff' },
    MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
})
