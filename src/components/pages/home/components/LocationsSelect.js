import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';

import { TourImgApi } from '../../../../../PostApi';


class LocationsSelect extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [0, 1, 2, 3, 4, 5],
        }
    }

    componentDidMount() {
        /************************* Fetch Data in PostApi *************************/
        TourImgApi().then((fetchData) => {
            //console.log(fetchData);
            this.setState({
                data: fetchData
            })
        })

    }


    render() {
        const { data } = this.state;
        return (
            <View style={{ backgroundColor: 'white', }}>
                <View style={styles.Container}>
                    <View style={{ width: '100%', }}>
                        <TouchableOpacity style={styles.TopContainer}
                            onPress={() => Actions.hightLine({
                                tour_id: data[0].tourcode,
                                keyword_id: data[0].keyword
                            })}
                        >
                            <Image
                                source={{ uri: data[0].pic_url }}
                                style={styles.TopImage}
                            />
                            <View style={styles.titleCenter}>
                                <Text style={styles.ImgTitle1}>{data[0].title}</Text>
                            </View>

                            <View style={styles.TextLeftTop}>
                                <Text style={{ color: '#fff' }}>RM {data[0].starter_price}</Text>
                            </View>

                        </TouchableOpacity>
                        <View style={styles.BottomContainer}>
                            <TouchableOpacity style={styles.BottomImage}
                                onPress={() => Actions.hightLine({
                                    tour_id: data[1].tourcode,
                                    keyword_id: data[1].keyword
                                })}
                            >
                                <Image
                                    source={{ uri: data[1].pic_url }}
                                    style={styles.BottomImageImg}
                                />
                                <View style={styles.titleCenter}>
                                    <Text style={styles.ImgTitle}>{data[1].title}</Text>
                                </View>
                                <View style={styles.TextLeftTop}>
                                    <Text style={{ color: '#fff' }}>RM {data[1].starter_price}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.BottomPaddingImage}
                                onPress={() => Actions.hightLine({
                                    tour_id: data[2].tourcode,
                                    keyword_id: data[2].keyword
                                })}
                            >
                                <Image
                                    source={{ uri: data[2].pic_url }}
                                    style={styles.BottomImageImg}
                                />
                                <View style={styles.titleCenter}>
                                    <Text style={styles.ImgTitle}>{data[2].title}</Text>
                                </View>
                                <View style={styles.TextLeftTop2}>
                                    <Text style={{ color: '#fff' }}>RM {data[2].starter_price}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.BottomPaddingImage}
                                onPress={() => Actions.hightLine({
                                    tour_id: data[3].tourcode,
                                    keyword_id: data[3].keyword
                                })}
                            >
                                <Image
                                    source={{ uri: data[3].pic_url }}
                                    style={styles.BottomImageImg}
                                />
                                <View style={styles.titleCenter}>
                                    <Text style={styles.ImgTitle}>{data[3].title}</Text>
                                </View>
                                <View style={styles.TextLeftTop2}>
                                    <Text style={{ color: '#fff' }}>RM {data[3].starter_price}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ width: '100%' }}>
                    <TouchableOpacity onPress={() => Actions.gdSeries()}>
                        <Text style={styles.ViewText}>{global.t('View_More')} ></Text>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
}



export default LocationsSelect;


const styles = StyleSheet.create({
    Container: { flex: 1, flexDirection: 'row', padding: 10, marginBottom: 55 },
    titleCenter: { position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', },
    ImgTitle1: {
        color: '#fff', fontSize: 30, fontWeight: 'bold', textShadowColor: 'black',
        textShadowOffset: { width: 1, height: 4 }, textShadowRadius: 5
    },
    ImgTitle: {
        color: '#fff', fontSize: 16, fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000',
    },
    //Left image
    TopContainer: { height: 168, },
    TopImage: { width: "100%", resizeMode: 'cover', flex: 1, borderRadius: 3, position: 'relative' },
    BottomContainer: { flex: 1, flexDirection: 'row', height: 40, width: '100%', paddingTop: 8, },
    BottomImage: { width: "33.33%", height: 80, },
    BottomImageImg: { flex: 1, width: "100%", resizeMode: 'cover', borderRadius: 3, position: 'relative' },
    BottomPaddingImage: { width: "33.33%", alignItems: 'flex-end', paddingLeft: 8, height: 80, },
    TextLeftTop: { position: 'absolute', top: 0, left: 0, padding: 3, paddingLeft: 5, backgroundColor: '#cb2431cf', },
    TextLeftTop2: { position: 'absolute', top: 0, left: 8, padding: 3, paddingLeft: 5, backgroundColor: '#cb2431cf', },

    //View more
    ViewText: { textAlign: 'center', color: '#de2d30', marginTop: -5, marginBottom: 13 }

})

