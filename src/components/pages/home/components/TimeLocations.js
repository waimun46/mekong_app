import React, { Component } from 'react';
import { StyleSheet, View, Platform, PermissionsAndroid, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconMAT from "react-native-vector-icons/MaterialIcons";

import Geocoder from 'react-native-geocoder';
import { WeatherWidget } from 'react-native-weather';

//const API_KEY = '7a522fdd67758b65b2eb6872f9f08d45';


class TimeLocations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      error: null,
      isLoading: true,
      temperature: 0,
      weatherCondition: null,
      locationName: null,
      data: [],
      catcherr: null
    };
  }



  componentDidMount() {

    this.getLocation();
    this.fetchWeather();


    /************************* Fetch Current Location *************************/
    // this.watchId = navigator.geolocation.watchPosition(
    //   (position) => {
    //     this.setState({
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       error: null,
    //     });
    //   },
    //   (error) => this.setState({ error: error.message }),
    //   { enableHighAccuracy: false, timeout: 20000 }
    // );

    /************************* Fetch Current Location *************************/
    // navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     this.setState({
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       error: null,
    //     });
    //   },
    //   (error) => this.setState({ error: error.message }),
    //   { enableHighAccuracy: false, timeout: 20000 }
    // );


    /************************* Fetch Weather in Current Locations *************************/
    //  navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     this.fetchWeather(position.coords.latitude, position.coords.longitude);
    //   },
    //   (error) => this.setState({ error: error.message }),
    //   { enableHighAccuracy: false, timeout: 500000 ,maximumAge: 3000 }

    // );


    //  /************************* Fetch Weather in Current Locations *************************/
    //  navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     this.fetchWeather(position.coords.latitude, position.coords.longitude);
    //   },
    //   (error) => this.setState({ error: error.message }),
    //   { enableHighAccuracy: false, timeout: 500000 ,maximumAge: 3000 }

    // );


    /************************* Alert Current Locations *************************/
    navigator.geolocation.getCurrentPosition(
      // function (position) {
      //   alert("Lat: " + position.coords.latitude + "\nLon: " + position.coords.longitude);
      // },
      function (error) {
        alert(error.message);
      }, {
        enableHighAccuracy: false, timeout: 500000,

      }
    );

    /************************* Display Current Date *************************/
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();

    this.setState({
      date:
        date + '/' + month + '/' + year,
    });



  }


  getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        }), () => { this.fetchWeather(); }
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 500000 },
    );
  }

  /************************* Fetch Weather in Api *************************/
  fetchWeather(lat = this.state.latitude, lon = this.state.longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
    console.log(lat, lon, '---------------lat long')
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then((response) => {
        console.log(response, 'response----------');
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then(json => {
        //alert(JSON.stringify(json));
        console.log(JSON.stringify(json));
        console.log("---------------");

        this.setState({
          temperature: json.main.temp.toFixed(0),
          weatherCondition: json.weather[0].main,
          locationName: json.name,
          isLoading: false,
          data: json.coord

        })
      })
      .catch((error) => {
        console.log(error, 'response----------error');
        this.setState({ 
          catcherr: error.message
        });
      });

  }

  // fetchWeather(lat = this.state.latitude, lon = this.state.longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
  //   console.log(lat, lon, '---------------lat long')
  //   fetch(
  //     `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
  //   )
  //     .then(res => res.json())
  //     .then(json => {
  //       //alert(JSON.stringify(json));
  //       console.log(JSON.stringify(json));
  //       console.log("---------------");

  //       this.setState({
  //         temperature: json.main.temp.toFixed(0),
  //         weatherCondition: json.weather[0].main,
  //         locationName: json.name,
  //         isLoading: false,

  //       });

  //     });

  // }


  /************************* Clear Previous Locations *************************/
  // componentWillUnmount() {
  //   navigator.geolocation.clearWatch(this.watchId);
  // }



  render() {
    const { isLoading, weatherCondition, temperature, locationName, latitude, longitude, data, catcherr } = this.state;
    console.log(latitude, longitude, '--------------------location fetch')
    return (
      <View style={{backgroundColor: 'white',}}>
        {/* <Text>state print---------</Text>
        <Text>latitude={this.state.latitude}, longitude={this.state.longitude}</Text>
        <Text>fetchWeather print---------</Text>
        <Text>latitude={data.lon}, longitude={data.lat}</Text>
        <Text>weatherCondition={weatherCondition}</Text>
        <Text>temperature={temperature}</Text>
        <Text>lat={data.lat}</Text>
        <Text>fetch catch error print-----------</Text>
        <Text>{catcherr ? catcherr : 'no error'}</Text> */}

        <View style={styles.ViewContainer}>

          {isLoading ? (

            <View style={styles.loadingContainer}>
              <ActivityIndicator size="small" color="#de2d30" />
            </View>

          ) : (

              <View style={styles.TextContainer} >
                <View >
                  <Text style={{ fontSize: 22 }} >{temperature}˚</Text>
                  {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
                </View>
                <View style={styles.DegreeContainer}>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 14 }} >{weatherCondition}</Text>
                  <Text style={{ marginTop: -3, fontSize: 14 }} numberOfLines={1} ellipsizeMode='tail'>{this.state.date}</Text>
                </View>
              </View>

            )}

          {/* 
        <View style={styles.TextContainer} >
          <View >
            <Text style={{ fontSize: 20 }} >32˚</Text>
          </View>
          <View style={styles.DegreeContainer}>
            <Text numberOfLines={1} ellipsizeMode='tail' >Sunny</Text>
            <Text style={{ marginTop: -3, }} numberOfLines={1} ellipsizeMode='tail'>{this.state.date}</Text>
          </View>
        </View>
       */}

          <View style={styles.TextContainer1} >
            <Text style={styles.HotTitle} uppercase={false}>{global.t('Hot')}</Text>
            <Text style={styles.PickTitle} uppercase={false} >{global.t('Pick')}</Text>
          </View>
          <View style={styles.LocationContainer}>
            <View
              style={styles.LocationStyle}
              numberOfLines={1}
              ellipsizeMode='tail'
            >
              <View style={styles.LocationTitle}>
                <IconMAT name="room" style={{ fontSize: 22, color: '#de2d30', }} />
                <Text ellipsizeMode='tail' style={styles.LocationText}></Text>

                {
                  isLoading ? (
                    <ActivityIndicator size="small" color="#de2d30" />
                  ) : (
                      <Text numberOfLines={1} ellipsizeMode='tail' style={styles.LocationText}>{locationName}</Text>
                    )
                }
              </View>

            </View>
          </View>

        </View>
      </View>
    );
  }
}

export default TimeLocations;

const styles = StyleSheet.create({
  TextContainer1: { flexDirection: 'row', justifyContent: 'space-between', width: '33.33%', paddingTop: 5 },
  TextContainer: { flexDirection: 'row', justifyContent: 'space-between', width: '33.33%', },
  ViewContainer: { padding: 10, flexDirection: 'row', justifyContent: 'space-between', },
  DegreeContainer: { textAlign: 'left', width: '70%', fontSize: 20, position: 'absolute', left: 40 },
  TextWarp: { paddingRight: 13, paddingLeft: 13, },
  HotTitle: { fontSize: 25, color: '#de2d30', fontWeight: 'bold', marginRight: 5, width: '50%', textAlign: 'right' },
  PickTitle: { fontSize: 25, color: 'black', fontWeight: 'bold', width: '50%', textAlign: 'left' },
  LocationContainer: { flexDirection: 'row', justifyContent: 'space-between', width: '33.33%', marginTop: 5 },
  LocationStyle: { fontSize: 25, width: '100%', lineHeight: 25, textAlign: 'right', },
  LocationTitle: { textAlign: 'right', flexDirection: 'row', justifyContent: 'flex-end', width: 80, alignSelf: 'flex-end' },
  LocationText: { fontSize: 16, textAlign: 'right', alignSelf: 'flex-end', },

})
