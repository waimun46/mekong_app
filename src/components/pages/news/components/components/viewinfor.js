import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, WebView, Dimensions, Linking } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'


/************************* IGNORED TAGS Api Data *************************/
const tags = _.without(IGNORED_TAGS,
    'table', 'caption', 'col', 'colgroup', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr',
)
/************************* Render Table *************************/
const renderers = {
    table: (x, c) => <View style={tableColumnStyle}>{c}</View>,
    col: (x, c) => <View style={tableColumnStyle}>{c}</View>,
    colgroup: (x, c) => <View style={tableRowStyle}>{c}</View>,
    tbody: (x, c) => <View style={tableColumnStyle}>{c}</View>,
    tfoot: (x, c) => <View style={tableRowStyle}>{c}</View>,
    th: (x, c) => <View style={thStyle}>{c}</View>,
    thead: (x, c) => <View style={tableRowStyle}>{c}</View>,
    caption: (x, c) => <View style={tableColumnStyle}>{c}</View>,
    tr: (x, c) => <View style={tableRowStyle}>{c}</View>,
    td: (x, c) => <View style={tdStyle}>{c}</View>,
    // iframe: (x, c) => <View style={imgStyle}>{c}</View>
}
/************************* Style Table *************************/
const tableDefaultStyle = {
    flex: 1,
    justifyContent: 'flex-start',
}

const tableColumnStyle = {
    ...tableDefaultStyle,
    flexDirection: 'column',
    alignItems: 'stretch'
}

const tableRowStyle = {
    ...tableDefaultStyle,
    flexDirection: 'row',
    alignItems: 'stretch',

}

const tdStyle = {
    ...tableDefaultStyle,
    padding: 2,
    backgroundColor: '#f6f8fa',
    borderWidth: .5,


}

const thStyle = {
    ...tdStyle,
    backgroundColor: '#fcf5f5',
    alignItems: 'center',
}
// const imgStyle = {
//    width: 1000,
//    height: 1000
// }


class ViewInfor extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        const htmlContent = this.props.data.description;
        console.log(this.props.data);

        return (

            <View style={{ backgroundColor: 'white' }}>

                <ScrollView>
                    <View style={styles.TextContainer}>
                        <Text style={styles.TitleText}>{this.props.data.title}</Text>
                        <View style={styles.ImageContainer}>
                            <Image source={{ uri: this.props.data.pic_url }} style={styles.ImageStyle} />
                        </View>
                        {/* <Text style={styles.TextStyle}>{this.props.data.description}</Text> */}

                        <HTML
                            alterChildren={(node) => {
                                if (node.name === 'iframe') {
                                    delete node.attribs.width;
                                    delete node.attribs.height;
                                }
                                return node.children;
                            }}
                            html={htmlContent}
                            imagesInitialDimensions={{ width: '100%', height: 480 }}
                            tagsStyles={{
                                p: { marginBottom: 30 },
                            }}
                            // classesStyles={{
                            //     'tg-uys7': { backgroundColor: 'gray', }
                            // }}
                            ignoredStyles={['font-family', 'display']}
                            ignoredTags={tags}
                            ignoredTags={[...IGNORED_TAGS, 'iframe']}
                            renderers={renderers}
                            onLinkPress={(https, href) => { Linking.openURL(https, href) }}
                            staticContentMaxWidth={Dimensions.get('window').width}
                            imagesMaxWidth={Dimensions.get('window').width}
                            debug={true}

                        />

                        {/* 
                        <WebView
                            ref={'webview'}
                            automaticallyAdjustContentInsets={false}
                            style={styles.webView}
                            html={htmlCode} />
                        />
                         <WebView
                        ref={'webview'}
                            scalesPageToFit={true}
                            source={{ html: this.props.data.description }}
                        //injectedJavaScript="window.postMessage(document.description)"
                        />
                        <WebView
                            style={styles.TextStyle}
                            source={{ uri: urlapi }}
                            domStorageEnabled={true}
                            startInLoadingState={true}
                            javaScriptEnabled={true}
                        />
                        */}


                    </View>
                </ScrollView>
            </View >


        );
    }
}


export default ViewInfor;

const styles = StyleSheet.create({
    TextContainer: { padding: 20 },
    TitleText: { textAlign: 'left', fontSize: 22, fontWeight: 'bold', color: '#de2d30', textTransform: 'uppercase' },
    TextStyle: { color: '#999', textAlign: 'left', fontSize: 16, paddingBottom: 20 },
    ImageContainer: { paddingTop: 20, paddingBottom: 20 },
    ImageStyle: { height: 250, width: '100%', resizeMode: "cover", },


})
