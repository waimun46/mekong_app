import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Dimensions } from 'react-native';
import { Container, Header, Content, Tab, Text, Tabs } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import EventNews from './components/eventnews';
import Blog from './components/blog';
import Promotions from './components/promotion';




class Newspage extends Component {

    render() {
        return (
            <View style={{ backgroundColor: 'white', }}>

                <ScrollView >
                    <View style={{ paddingBottom: 30 }}>
                        <Tabs
                            tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}
                            style={{ backgroundColor: 'white', }}
                            //locked
                        >
                        
                            {/*************************** BLOG ***************************/}
                            <Tab
                                heading={global.t('BLOG')}
                                tabStyle={{ backgroundColor: "white", }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <Blog />
                            </Tab>

                            {/*************************** PROMOTION ***************************/}
                            <Tab
                                heading={global.t('PROMOTION')}
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <Promotions />
                            </Tab>

                            {/*************************** EVENTS_NEWS ***************************/}
                            <Tab
                                heading={global.t('EVENTS_NEWS')}
                                tabStyle={{ backgroundColor: "white", }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <EventNews />
                            </Tab>




                        </Tabs>
                    </View>
                </ScrollView>

            </View>
        );
    }
}


export default Newspage;

const styles = StyleSheet.create({

})
